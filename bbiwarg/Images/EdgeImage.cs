﻿using BBIWARG.Utility;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Drawing;

namespace BBIWARG.Images
{
    /// <summary>
    /// EdgeImage creates an edge image form a depth image and stores it as an <see cref="Image"/>
    /// </summary>
    public class EdgeImage
    {
        /// <summary>
        /// the edge image
        /// </summary>
        public Image<Gray, Byte> Image { get; private set; }

        /// <summary>
        /// dilated version of the edge image
        /// </summary>
        public Image<Gray, Byte> RoughImage { get; private set; }

        /// <summary>
        /// the size of the edge image
        /// </summary>
        public ImageSize Size { get; private set; }

        /// <summary>
        /// Constructs a new EdgeImage from a depth image.
        /// </summary>
        /// <param name="depthImage">the depth image</param>
        public EdgeImage(DepthImage depthImage)
        {
            Size = depthImage.Size;

            Image = depthImage.Image.ConvertScale<byte>(255f / (depthImage.MaxDepth - depthImage.MinDepth), 0);
            Image = Image.Canny(Parameters.EdgeImageCannyStartThreshold, Parameters.EdgeImageCannyLinkingThreshold, Parameters.EdgeImageCannySize);
            Image = Image.ThresholdBinary(new Gray(0), new Gray(1));

            RoughImage = Image.Dilate(Parameters.EdgeImageRoughNumDilationIterations);
        }

        /// <summary>
        /// Creates a new EdgeImage from the data of another edge image.
        /// </summary>
        /// <param name="edgeImage">the edge image</param>
        /// <param name="roughEdgeImage">dilated version of the edge image</param>
        /// <param name="size">size of the edge image</param>
        public EdgeImage(Image<Gray, Byte> edgeImage, Image<Gray, Byte> roughEdgeImage, ImageSize size)
        {
            Size = size;
            Image = edgeImage;
            RoughImage = roughEdgeImage;
        }

        /// <summary>
        /// Returns a copy of this edge image.
        /// </summary>
        /// <returns>copy of this edge image</returns>
        public EdgeImage copy()
        {
            return new EdgeImage(Image.Copy(), RoughImage.Copy(), Size);
        }

        /// <summary>
        /// Returns the first vector starting at start and going in the given direction, which rounds to a point containing an edge in the rough edge image
        /// or null if no such vector is found.
        /// </summary>
        /// <param name="start">start position</param>
        /// <param name="direction">search direction</param>
        /// <param name="maxSearchSize">maximum number of steps in direction</param>
        /// <returns>the positions of the next rough edge</returns>
        public Vector2D findNextRoughEdge(Vector2D start, Vector2D direction, int maxSearchSize = 0)
        {
            Vector2D maxGrow = (Size.MaxPixel - start) / direction;
            Vector2D maxDecline = start / direction.getAbsolute();

            int maxStepsX;
            if (direction.X > 0)
                maxStepsX = maxGrow.IntX;
            else if (direction.X < 0)
                maxStepsX = maxDecline.IntX;
            else
                maxStepsX = int.MaxValue;

            int maxStepsY;
            if (direction.Y > 0)
                maxStepsY = maxGrow.IntY;
            else if (direction.Y < 0)
                maxStepsY = maxDecline.IntY;
            else
                maxStepsY = int.MaxValue;

            int maxSteps = Math.Min(maxStepsX, maxStepsY);

            if (maxSearchSize != 0)
                maxSteps = Math.Min(maxSteps, maxSearchSize);

            Vector2D end = start.copy();
            for (int i = 0; i < maxSteps; i++)
            {
                end += direction;

                if (isRoughEdgeAt(end))
                    return end;
            }

            return null;
        }

        /// <summary>
        /// Returns true iff an edge is at a point.
        /// </summary>
        /// <param name="point">the point</param>
        /// <returns>true iff an edge is at point</returns>
        public bool isEdgeAt(Point point)
        {
            return isEdgeAt(point.X, point.Y);
        }

        /// <summary>
        /// Returns true iff an edge is at a position.
        /// </summary>
        /// <param name="x">x coordinate of the position</param>
        /// <param name="y">y coordinate of the position</param>
        /// <returns>true iff an edge is at the position</returns>
        public bool isEdgeAt(int x, int y)
        {
            return Image.Data[y, x, 0] > 0;
        }

        /// <summary>
        /// Returns true iff an edge is at a point in the rough edge image.
        /// </summary>
        /// <param name="point">the point</param>
        /// <returns>true iff an edge is at point in the rough edge image</returns>
        public bool isRoughEdgeAt(Point point)
        {
            return isRoughEdgeAt(point.X, point.Y);
        }

        /// <summary>
        /// Returns true iff an edge is at a position in the rough edge image.
        /// </summary>
        /// <param name="x">x coordinate of the position</param>
        /// <param name="y">y coordinate of the position</param>
        /// <returns>true iff an edge is at the position in the rough edge image</returns>
        public bool isRoughEdgeAt(int x, int y)
        {
            return RoughImage.Data[y, x, 0] > 0;
        }

        /// <summary>
        /// Removes all edges inside the given polygon in both edge images.
        /// </summary>
        /// <param name="polygon">the polygon</param>
        public void removeEdgesInsidePolygon(Point[] polygon)
        {
            Image.FillConvexPoly(polygon, new Gray(0));
            RoughImage.FillConvexPoly(polygon, new Gray(0));
        }
    }
}