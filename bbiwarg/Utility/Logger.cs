﻿using System;

namespace BBIWARG.Utility
{
    /// <summary>
    /// flags describing the different log subjects
    /// </summary>
    public enum LogSubject
    {
        None = 0,
        FingerDetector = 1,
        FingerTracker = 2,
        HandDetector = 4,
        PalmDetector = 8,
        TouchDetector = 16,
        TouchTracker = 32,
        TUIOServer = 64,
        TouchEvents = 128,
        VideoControls = 256,
        HandTracker = 512,
        PalmTracker = 1024
    }

    /// <summary>
    /// Logs messages.
    /// </summary>
    internal static class Logger
    {
        /// <summary>
        /// the current frame
        /// </summary>
        public static int CurrentFrame { get; set; }

        /// <summary>
        /// May print a message depending on the subject and <see cref="Parameters.LoggerEnabledSubjects"/>.
        /// </summary>
        /// <param name="message">the message</param>
        /// <param name="subject">the subject the message belongs to, determines if this message is printed</param>
        public static void log(string message, LogSubject subject)
        {
            if (Parameters.LoggerEnabledSubjects.HasFlag(subject))
                Console.WriteLine("Frame: " + CurrentFrame + "\t" + message);
        }
    }
}