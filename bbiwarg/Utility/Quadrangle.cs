﻿using System;

namespace BBIWARG.Utility
{
    /// <summary>
    /// Class to represent a quadrangle in 2 dimensions, a quadrangle is a geometric shape composed of 4 arbitrary points.
    /// </summary>
    public class Quadrangle
    {
        /// <summary>
        /// point in the bottom left corner
        /// </summary>
        public Vector2D BottomLeft { get; private set; }

        /// <summary>
        /// point in the bottom right corner
        /// </summary>
        public Vector2D BottomRight { get; private set; }

        /// <summary>
        /// all 4 points of the quadrangle in clockwise order, beginning at the top left corner
        /// </summary>
        public Vector2D[] Corners { get { return new Vector2D[4] { TopLeft, TopRight, BottomRight, BottomLeft }; } }

        /// <summary>
        /// point in the top left corner
        /// </summary>
        public Vector2D TopLeft { get; private set; }

        /// <summary>
        /// point in the top right corner
        /// </summary>
        public Vector2D TopRight { get; private set; }

        /// <summary>
        /// Standard constructor of quadrangle, which sets the 4 points.
        /// </summary>
        /// <param name="topLeft">top left point <see cref="topLeft"/></param>
        /// <param name="topRight">top right point <see cref="topRight"/></param>
        /// <param name="bottomRight">bottom right point <see cref="bottomRight"/></param>
        /// <param name="bottomLeft">bottom left point <see cref="bottomLeft"/></param>
        public Quadrangle(Vector2D topLeft, Vector2D topRight, Vector2D bottomRight, Vector2D bottomLeft)
        {
            TopLeft = topLeft;
            TopRight = topRight;
            BottomRight = bottomRight;
            BottomLeft = bottomLeft;
        }

        /// <summary>
        /// Computes the relative position of a point inside the quadrangle, iff the point is inside the output varies between (0,0) and (1,1), else it is smaller or greater.
        /// </summary>
        /// <param name="p">the point which relative position should be computed</param>
        /// <returns>a point between (0,0) and (1,1), iff the point is inside the quadrangle</returns>
        public Vector2D getRelativePosition(Vector2D p)
        {
            Vector2D a, b, c, d;

            a = TopLeft;
            b = TopRight;
            c = BottomRight;
            d = BottomLeft;

            float vc = (a.Y - p.Y) * (d.X - p.X) - (a.X - p.X) * (d.Y - p.Y);
            float vb = (a.Y - p.Y) * (c.X - d.X) + (b.Y - a.Y) * (d.X - p.X) - (a.X - p.X) * (c.Y - d.Y) - (b.X - a.X) * (d.Y - p.Y);
            float va = (b.Y - a.Y) * (c.X - d.X) - (b.X - a.X) * (c.Y - d.Y);

            float vd = vb * vb - 4 * va * vc;

            float u = (-vb - (float)Math.Sqrt(vd)) / (2 * va);

            float p1x = a.X + (b.X - a.X) * u;
            float p2x = d.X + (c.X - d.X) * u;
            float px = p.X;

            float v = (px - p1x) / (p2x - p1x);

            return new Vector2D(u, v);
        }

        /// <summary>
        /// Checks whether a point is inside the quadrangle or not, a tolerance can be set, which allows that the decision is based on a quadrangle which is tolerance times greater/smaller than the original quadrangle.
        /// </summary>
        /// <param name="point">the point</param>
        /// <param name="tolerance">the tolerance value</param>
        /// <returns>true iff the point is inside the quadrangle(plus tolerance)</returns>
        public bool isInside(Vector2D point, float tolerance = 0.0f)
        {
            Vector2D relativePos = getRelativePosition(point);
            float min = 0 - tolerance;
            float max = 1 + tolerance;

            return (relativePos.X >= min && relativePos.X <= max) && (relativePos.Y >= min && relativePos.Y <= max);
        }
    }
}