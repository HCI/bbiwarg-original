﻿namespace BBIWARG.Utility
{
    /// <summary>
    /// Manages the size of a image.
    /// </summary>
    public class ImageSize
    {
        /// <summary>
        /// the length of the longer image diagonal
        /// </summary>
        public float DiagonalLength { get { return MaxPixel.Length; } }

        /// <summary>
        /// image height
        /// </summary>
        public int Height { get; private set; }

        /// <summary>
        /// position in the image with maximum x and y values
        /// </summary>
        public Vector2D MaxPixel { get; private set; }

        /// <summary>
        /// number of pixels in the image
        /// </summary>
        public int NumPixels { get { return Width * Height; } }

        /// <summary>
        /// image width
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// Constructs a ImageSize.
        /// </summary>
        /// <param name="width">image width</param>
        /// <param name="height">image height</param>
        public ImageSize(int width, int height)
        {
            Width = width;
            Height = height;
            MaxPixel = new Vector2D(width - 1, height - 1);
        }

        /// <summary>
        /// Computes an absolute point in the image.
        /// </summary>
        /// <param name="relativePoint">relative point (x and y in [0,1])</param>
        /// <returns>absolute point</returns>
        public Vector2D getAbsolutePoint(Vector2D relativePoint)
        {
            return relativePoint.scale(MaxPixel);
        }

        /// <summary>
        /// Computes a relative point in the image.
        /// </summary>
        /// <param name="absolutePoint">absolute point in the image</param>
        /// <returns>relative point (x and y in [0,1])</returns>
        public Vector2D getRelativePoint(Vector2D absolutePoint)
        {
            return new Vector2D(absolutePoint.X / Width, absolutePoint.Y / Height);
        }
    }
}