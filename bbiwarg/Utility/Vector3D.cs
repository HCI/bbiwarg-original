﻿using System;

namespace BBIWARG.Utility
{
    /// <summary>
    /// Class with represents a vector or a point in 3 dimensional space.
    /// </summary>
    public class Vector3D
    {
        /// <summary>
        /// X component as integer
        /// </summary>
        public int IntX { get { return (int)X; } }

        /// <summary>
        /// Y component as Integer
        /// </summary>
        public int IntY { get { return (int)Y; } }

        /// <summary>
        /// Z component as Integer
        /// </summary>
        public int IntZ { get { return (int)Z; } }

        /// <summary>
        /// length of the vector, computed with euclidean distance (2.Norm)
        /// </summary>
        public float Length { get { return (float)Math.Sqrt(X * X + Y * Y + Z * Z); } }

        /// <summary>
        /// X (first) component
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Y (second) component
        /// </summary>
        public float Y { get; private set; }

        /// <summary>
        /// Z (third) component
        /// </summary>
        public float Z { get; private set; }

        /// <summary>
        /// Standard constructor which sets the 3 components.
        /// </summary>
        /// <param name="x">first component</param>
        /// <param name="y">second component</param>
        /// <param name="z">third component</param>
        public Vector3D(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        /// <summary>
        /// Subtracts two vectors (component-by-component).
        /// </summary>
        /// <param name="v1">the minuend</param>
        /// <param name="v2">the subtrahend</param>
        /// <returns>the difference of the two vectors</returns>
        public static Vector3D operator -(Vector3D v1, Vector3D v2)
        {
            return new Vector3D(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        /// <summary>
        /// Multiplies this vector with a scalar value.
        /// </summary>
        /// <param name="scalar">the value</param>
        /// <param name="v">this vector</param>
        /// <returns>the multiplied vector</returns>
        public static Vector3D operator *(float scalar, Vector3D v)
        {
            return new Vector3D(scalar * v.X, scalar * v.Y, scalar * v.Z);
        }

        /// <summary>
        /// Multiplies this vector with a scalar value.
        /// </summary>
        /// <param name="v">this vector</param>
        /// <param name="scalar">the value</param>
        /// <returns>the multiplied vector</returns>
        public static Vector3D operator *(Vector3D v, float scalar)
        {
            return new Vector3D(scalar * v.X, scalar * v.Y, scalar * v.Z);
        }

        /// <summary>
        /// Divides this vector with a scalar value.
        /// </summary>
        /// <param name="v">this vector</param>
        /// <param name="scalar">the value</param>
        /// <returns>the divided vector</returns>
        public static Vector3D operator /(Vector3D v, float scalar)
        {
            return new Vector3D(v.X / scalar, v.Y / scalar, v.Z / scalar);
        }

        /// <summary>
        /// Divides on vector with another vector component-by-component.
        /// </summary>
        /// <param name="v1">the dividend</param>
        /// <param name="v2">the divisor</param>
        /// <returns>the component divided vector</returns>
        public static Vector3D operator /(Vector3D v1, Vector3D v2)
        {
            return new Vector3D(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z);
        }

        /// <summary>
        /// Adds two vectors (component-by-component).
        /// </summary>
        /// <param name="v1">first addend</param>
        /// <param name="v2">second addend</param>
        /// <returns>sum of the vectors</returns>
        public static Vector3D operator +(Vector3D v1, Vector3D v2)
        {
            return new Vector3D(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        /// <summary>
        /// Computes the distance from the point this vector describes to another 3D point (vector).
        /// The distance is the euclidean distance (2. Norm).
        /// </summary>
        /// <param name="point">the other point</param>
        /// <returns>euclidean distance between this and point</returns>
        public float getDistanceTo(Vector3D point)
        {
            return (this - point).Length;
        }
    }
}