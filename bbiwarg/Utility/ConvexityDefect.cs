﻿using BBIWARG.Recognition.FingerRecognition;
using Emgu.CV.Structure;

namespace BBIWARG.Utility
{
    /// <summary>
    /// Stores data about and provides functions to work with convexity defects.
    /// </summary>
    public class ConvexityDefect
    {
        /// <summary>
        /// distance from OuterLineSegment to Inner
        /// </summary>
        public float Depth { get; private set; }

        /// <summary>
        /// the depth point
        /// </summary>
        public Vector2D Inner { get; private set; }

        /// <summary>
        /// line segment from start to end point
        /// </summary>
        public LineSegment2D OuterLineSegment { get; private set; }

        /// <summary>
        /// the point of start and end point which is farther away from the depth point
        /// </summary>
        public Vector2D OuterLong { get; private set; }

        /// <summary>
        /// the point of start and end point which is nearer to the depth point
        /// </summary>
        public Vector2D OuterShort { get; private set; }

        /// <summary>
        /// vector from Inner to OuterLong
        /// </summary>
        public Vector2D VectorLong { get; private set; }

        /// <summary>
        /// vector from Inner to OuterShort
        /// </summary>
        public Vector2D VectorShort { get; private set; }

        /// <summary>
        /// Constructs a ConvexityDefect.
        /// </summary>
        /// <param name="mcvConvexityDefect">the emgu convexity defect which has start, end and depth point</param>
        public ConvexityDefect(MCvConvexityDefect mcvConvexityDefect)
        {
            Inner = new Vector2D(mcvConvexityDefect.DepthPoint);
            Vector2D p1 = new Vector2D(mcvConvexityDefect.StartPoint);
            Vector2D p2 = new Vector2D(mcvConvexityDefect.EndPoint);

            if ((p1 - Inner).Length > (p2 - Inner).Length)
            {
                OuterLong = p1;
                OuterShort = p2;
            }
            else
            {
                OuterLong = p2;
                OuterShort = p1;
            }

            VectorLong = OuterLong - Inner;
            VectorShort = OuterShort - Inner;
            OuterLineSegment = new LineSegment2D(OuterLong, OuterShort);
            Depth = OuterLineSegment.getDistanceTo(Inner);
        }

        /// <summary>
        /// Tests if this defect is caused by a finger.
        /// </summary>
        /// <param name="finger">the finger maybe causing the defect</param>
        /// <returns>true iff this defect is caused by finger</returns>
        public bool isCausedByFinger(Finger finger)
        {
            return OuterLineSegment.intersectsWith(finger.LineSegment);
        }

        /// <summary>
        /// Tests if this defect could be the defect near the thumb.
        /// </summary>
        /// <param name="thumb">finger representing the thumb</param>
        /// <returns>true iff this defect is a possible thumb defect</returns>
        public bool isPossibleThumbDefect(Finger thumb)
        {
            float tipDistance = thumb.TipPoint.getDistanceTo(OuterShort);
            float handDistance = thumb.HandPoint.getDistanceTo(Inner);
            float thumbShortLengthRatio = thumb.LineSegment.Length / VectorShort.Length;
            float shortLongLengthRatio = VectorShort.Length / VectorLong.Length;

            return tipDistance <= Parameters.HandThumbDefectMaxDistanceToThumb &&
                    handDistance <= Parameters.HandThumbDefectMaxDistanceToThumb &&
                    thumbShortLengthRatio <= Parameters.HandThumbDefectMaxThumbShortLengthRatio && thumbShortLengthRatio >= Parameters.HandThumbDefectMinThumbShortLengthRatio &&
                    shortLongLengthRatio <= Parameters.HandThumbDefectMaxShortLongLengthRatio && shortLongLengthRatio >= Parameters.HandThumbDefectMinShortLongLengthRatio;
        }
    }
}