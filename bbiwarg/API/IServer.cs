﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BBIWARG.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServer" in both code and config file together.
    [ServiceContract]
    public interface IServer
    {
        [OperationContract]
        Grid getGrid();
        [OperationContract]
        string setGrid(Grid g);

        [OperationContract]
        Slider getSlider();
        [OperationContract]

        string setSlider(Slider s);
    }
}
