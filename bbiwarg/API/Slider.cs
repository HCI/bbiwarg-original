﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBIWARG.API
{
    public class Slider
    {
        public string Type { get; set; }
        public int Pos { get; set; }
        public int Max { get; set; }
        public int Cur { get; set; }

        public static Slider instance;
        public static Slider getInctance()
        {
            if (instance == null)
                instance = new Slider();
            return instance;
        }
        private Slider()
        {
            Type = "slider";
            Pos = 0;
            Max = 0;
            Cur = 0;
        }
    }
}
