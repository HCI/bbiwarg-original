﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Windows.Forms;

namespace BBIWARG.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Server" in both code and config file together.
    public class Server : IServer
    {
        [WebInvoke(Method = "GET",
                     ResponseFormat = WebMessageFormat.Json,
                     UriTemplate = "grid")]
        public Grid getGrid()
        {
            return Grid.getInctance();
        }

        [WebInvoke(Method = "POST",
                      RequestFormat = WebMessageFormat.Json,
                     ResponseFormat = WebMessageFormat.Json,
                     UriTemplate = "grid")]
        public string setGrid(Grid grid)
        {
            try
            {
                Grid.getInctance().Cols = grid.Cols;
                Grid.getInctance().Rows = grid.Rows;
                /*Output.DebugOutput.DebugWindow.palmGridNumColumnsTrackBar.Invoke((MethodInvoker)delegate {
                    Output.DebugOutput.DebugWindow.palmGridNumColumnsTrackBar.Value = grid.Cols;
                });
                Output.DebugOutput.DebugWindow.palmGridNumRowsTrackBar.Invoke((MethodInvoker)delegate {
                    Output.DebugOutput.DebugWindow.palmGridNumRowsTrackBar.Value = grid.Rows;
                });*/
                return "Done!";
            }
            catch(Exception e)
            { return e.ToString(); }
        }


        [WebInvoke(Method = "GET",
                     ResponseFormat = WebMessageFormat.Json,
                     UriTemplate = "slider")]
        public Slider getSlider()
        {
            Slider.getInctance().Cur = Parameters.PalmSliderCurr;
            Slider.getInctance().Pos = Parameters.PalmSliderPos;
            Slider.getInctance().Max = Parameters.PalmSliderMax;
            return Slider.getInctance();
        }

        [WebInvoke(Method = "POST",
                      RequestFormat = WebMessageFormat.Json,
                     ResponseFormat = WebMessageFormat.Json,
                     UriTemplate = "slider")]
        public string setSlider(Slider slider)
        {
            try
            {
                Slider.getInctance().Cur = slider.Cur;
                Slider.getInctance().Pos = slider.Pos;
                Slider.getInctance().Max = slider.Max;
                Parameters.PalmSliderCurr = slider.Cur;
                Parameters.PalmSliderPos = slider.Pos;
                Parameters.PalmSliderMax = slider.Max;
                return "Done!";
            }
            catch (Exception e)
            { return e.ToString(); }
        }
    }
}
