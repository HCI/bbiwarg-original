﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BBIWARG.API
{
    public class Grid
    {
        public string Type { get; set; }
        public int Rows { get; set; }
        public int Cols { get; set; }

        public static Grid instance;
        public static Grid getInctance()
        {
            if (instance == null)
                instance = new Grid();
            return instance;
        }
        private Grid()
        {
            Type = "grid";
            Rows = 0;
            Cols = 0;
        }
    }
}
