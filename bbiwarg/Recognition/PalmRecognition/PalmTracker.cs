﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using System.Collections.Generic;

namespace BBIWARG.Recognition.PalmRecognition
{
    /// <summary>
    /// Keeps track of palms over a period of time.
    /// </summary>
    internal class PalmTracker : Tracker<Palm, TrackedPalm>
    {
        /// <summary>
        /// Initializes a new instance of the PalmTracker class.
        /// </summary>
        /// <param name="imageSize">Size of the input image.</param>
        public PalmTracker(ImageSize imageSize)
            : base(imageSize)
        {
        }

        /// <summary>
        /// Calculates the similarity [0-1] of a tracked palm and a detected palm
        /// </summary>
        /// <param name="trackedPalm">the tracked palm</param>
        /// <param name="detectedPalm">the detected palm</param>
        /// <returns>the similarity</returns>
        public override float calculateSimilarity(TrackedPalm trackedPalm, Palm detectedPalm)
        {
            float handSimilarity = (detectedPalm.Hand.TrackID == trackedPalm.LastObject.Hand.TrackID) ? 1 : 0;
            float wristUpperSimilarity = getPositionSimilarity(trackedPalm.WristUpperPrediction, detectedPalm.WristUpper, Parameters.PalmTrackerMaxWristUpperRelativeMove);
            float wristLowerSimilarity = getPositionSimilarity(trackedPalm.WristLowerPrediction, detectedPalm.WristLower, Parameters.PalmTrackerMaxWristLowerRelativeMove);
            float fingersUpperSimilarity = getPositionSimilarity(trackedPalm.FingersUpperPrediction, detectedPalm.FingersUpper, Parameters.PalmTrackerMaxFingersUpperRelativeMove);
            float fingersLowerSimilarity = getPositionSimilarity(trackedPalm.FingersLowerPrediction, detectedPalm.FingersLower, Parameters.PalmTrackerMaxFingersLowerRelativeMove);

            return handSimilarity * wristUpperSimilarity * wristLowerSimilarity * fingersUpperSimilarity * fingersLowerSimilarity;
        }

        /// <summary>
        /// Updates the tracked palms with the detected palms in the current frame and stores the (optimized) results in frameData.trackedPalms
        /// </summary>
        /// <param name="frameData">the current frame</param>
        public void trackPalms(FrameData frameData)
        {
            trackObjects(frameData.DetectedPalms);
            frameData.TrackedPalms = getOptimizedPalms();
        }

        /// <summary>
        /// Creates a TrackedPalm
        /// </summary>
        /// <param name="detectedPalm">the detected palm</param>
        /// <returns>a new TrackedPalm</returns>
        protected override TrackedPalm createTrackedObject(Palm detectedPalm)
        {
            return new TrackedPalm(idPool.getNextUnusedID(), detectedPalm, Parameters.PalmTrackerNumFramesDetectedUntilTracked, Parameters.PalmTrackerNumFramesLostUntilDeleted);
        }

        /// <summary>
        /// Gets all optimized representations of all tracked palms.
        /// </summary>
        /// <returns>all optimized tracked palms</returns>
        private List<Palm> getOptimizedPalms()
        {
            List<Palm> optimizedPalms = new List<Palm>();
            foreach (TrackedPalm tp in TrackedObjects)
            {
                if (tp.CurrentState == TrackingState.Tracked)
                    optimizedPalms.Add(tp.OptimizedPalm);
            }
            return optimizedPalms;
        }
    }
}