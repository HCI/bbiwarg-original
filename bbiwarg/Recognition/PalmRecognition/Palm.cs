﻿using BBIWARG.Recognition.HandRecognition;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using System;

namespace BBIWARG.Recognition.PalmRecognition
{
    /// <summary>
    /// The handedness of the palm.
    /// </summary>
    public enum HandSide
    {
        Undefined = 0,
        Right = 1,
        Left = 2
    }

    /// <summary>
    /// Represents a palm (each hand with one finger (thumb) has palm)
    /// </summary>
    public class Palm : TrackableObject
    {
        /// <summary>
        /// the position of the fingers lower (bottom right for left hand, bottom left for right hand)
        /// </summary>
        public Vector2D FingersLower { get; private set; }

        /// <summary>
        /// the position of the upper fingers (top right for left hand, top left for left hand)
        /// </summary>
        public Vector2D FingersUpper { get; private set; }

        /// <summary>
        /// the hand belonging to this palm
        /// </summary>
        public Hand Hand { get; private set; }

        /// <summary>
        /// the handedness
        /// </summary>
        public HandSide HandSide { get; private set; }

        /// <summary>
        /// the quadrangle of the four palm points
        /// </summary>
        public Quadrangle Quad { get; private set; }

        /// <summary>
        /// the thumb's convexity defect
        /// </summary>
        public ConvexityDefect ThumbDefect { get; private set; }

        /// <summary>
        /// the position of the wrist lower (bottom left for left hand, bottom right for right hand)
        /// </summary>
        public Vector2D WristLower { get; private set; }

        /// <summary>
        /// the position of the upper wrist (top left corner for left hand, top right for right hand)
        /// </summary>
        public Vector2D WristUpper { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Palm class.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <param name="thumbDefect">The thumb defect.</param>
        /// <param name="handSide">The handedness.</param>
        /// <param name="wristUpper">The wrist upper position.</param>
        /// <param name="fingersUpper">The fingers upper position.</param>
        /// <param name="fingersLower">The fingers lower position.</param>
        /// <param name="wristLower">The wrist lower position.</param>
        public Palm(Hand hand, ConvexityDefect thumbDefect, HandSide handSide, Vector2D wristUpper, Vector2D fingersUpper, Vector2D fingersLower, Vector2D wristLower)
        {
            Hand = hand;
            ThumbDefect = thumbDefect;
            HandSide = handSide;
            WristUpper = wristUpper;
            FingersUpper = fingersUpper;
            FingersLower = fingersLower;
            WristLower = wristLower;

            hand.Palm = this;

            createQuad();
        }

        /// <summary>
        /// Gets the relative position [0-1;0-1] from an absolute position.
        /// </summary>
        /// <param name="absolutePosition">the absolute position</param>
        /// <returns>the relative position</returns>
        public Vector2D getRelativePosition(Vector2D absolutePosition)
        {
            Vector2D relativePosition = Quad.getRelativePosition(absolutePosition);
            float x = Math.Max(0, Math.Min(1, relativePosition.X));
            float y = Math.Max(0, Math.Min(1, relativePosition.Y));

            return new Vector2D(x, y);
        }

        /// <summary>
        /// Checks if the position is inside the palm (with a tolerance)
        /// </summary>
        /// <param name="position">the absolute position</param>
        /// <returns>whether the position is inside the palm</returns>
        public bool isInside(Vector2D position)
        {
            return Quad.isInside(position, Parameters.PalmInsideTolerance);
        }

        /// <summary>
        /// Creates the palm quadrangle
        /// </summary>
        private void createQuad()
        {
            if (HandSide == HandSide.Left)
                Quad = new Quadrangle(WristUpper, FingersUpper, FingersLower, WristLower);
            else
                Quad = new Quadrangle(FingersUpper, WristUpper, WristLower, FingersLower);
        }
    }
}