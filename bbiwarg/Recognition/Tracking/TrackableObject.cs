﻿namespace BBIWARG.Recognition.Tracking
{
    /// <summary>
    /// A object that can be identified via a unique ID.
    /// </summary>
    public class TrackableObject
    {
        /// <summary>
        /// the track id
        /// </summary>
        public int TrackID { get; private set; }

        /// <summary>
        /// Sets the object's track id.
        /// </summary>
        /// <param name="id">the track id</param>
        public void setTracked(int id)
        {
            TrackID = id;
        }
    }
}