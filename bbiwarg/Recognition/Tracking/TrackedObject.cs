﻿namespace BBIWARG.Recognition.Tracking
{
    /// <summary>
    /// The possible tracking state values
    /// </summary>
    public enum TrackingState
    {
        Undefined = 0,
        Detected = 1,
        Tracked = 2,
        Lost = 3,
        Deleted = 4
    }

    /// <summary>
    /// A tracked object is identified by several TrackableObjects in different frames that have a high similarity value and are considered to represent the same object over time.
    /// </summary>
    /// <typeparam name="T">The type of the trackable object</typeparam>
    public abstract class TrackedObject<T> where T : TrackableObject
    {
        /// <summary>
        /// the number of consecutive frames the object has to be detected until its state changes to tracked
        /// </summary>
        private int numFramesDetectedUntilTracked;

        /// <summary>
        /// the number of consecutive frames the object has to be lost until its state changes to delete
        /// </summary>
        private int numFramesLostUntilDeleted;

        /// <summary>
        /// indicates whether the object has been tracked before
        /// </summary>
        private bool wasTrackedBefore;

        /// <summary>
        /// a reference to the current TrackableObject
        /// </summary>
        public T CurrentObject { get; private set; }

        /// <summary>
        /// the current tracking state
        /// </summary>
        public TrackingState CurrentState { get; private set; }

        /// <summary>
        /// the objects track id
        /// </summary>
        public int ID { get; private set; }

        /// <summary>
        /// a reference to the last TrackableObject
        /// </summary>
        public T LastObject { get; private set; }

        /// <summary>
        /// the number of consecutive frames in the current state
        /// </summary>
        public int NumFramesInCurrentState { get; private set; }

        /// <summary>
        /// the previous tracking state
        /// </summary>
        public TrackingState PreviousState { get; private set; }

        /// <summary>
        /// Initializes a new instance of the TrackedObject class.
        /// </summary>
        /// <param name="id">The track id.</param>
        /// <param name="detectedObject">The detected object.</param>
        /// <param name="numFramesDetectedUntilTracked">The number of consecutive frames detected until it is considered to be tracked.</param>
        /// <param name="numFramesLostUntilDeleted">The number of consecutive frames lost until the object will be deleted.</param>
        public TrackedObject(int id, T detectedObject, int numFramesDetectedUntilTracked, int numFramesLostUntilDeleted)
        {
            ID = id;
            this.numFramesDetectedUntilTracked = numFramesDetectedUntilTracked;
            this.numFramesLostUntilDeleted = numFramesLostUntilDeleted;
            wasTrackedBefore = false;

            detectedObject.setTracked(ID);

            CurrentObject = detectedObject;
            LastObject = detectedObject;
            CurrentState = TrackingState.Detected;
            PreviousState = TrackingState.Undefined;
            NumFramesInCurrentState = 1;
        }

        /// <summary>
        /// Adds the detectedObject to the tracking history and updates the current state.
        /// </summary>
        /// <param name="detectedObject">the best fitting trackableObject in the current frame</param>
        public virtual void updateFrame(T detectedObject)
        {
            if (detectedObject != null)
            {
                detectedObject.setTracked(ID);
                LastObject = detectedObject;
            }

            TrackingState newState = getNewState(detectedObject);

            if (!wasTrackedBefore && newState == TrackingState.Tracked)
                wasTrackedBefore = true;

            PreviousState = CurrentState;
            CurrentState = newState;
            CurrentObject = detectedObject;

            if (PreviousState == newState)
                NumFramesInCurrentState++;
            else
                NumFramesInCurrentState = 1;
        }

        /// <summary>
        /// Calculates the new tracking state given the new trackableObject.
        /// </summary>
        /// <param name="detectedObject">the best fitting trackableObject in the current frame</param>
        /// <returns>the new TrackingState</returns>
        private TrackingState getNewState(T detectedObject)
        {
            TrackingState newState = TrackingState.Undefined;

            if (detectedObject != null)
            {
                switch (CurrentState)
                {
                    case TrackingState.Lost:
                        if (wasTrackedBefore)
                            newState = TrackingState.Tracked;
                        else
                            newState = TrackingState.Detected;
                        break;

                    case TrackingState.Tracked:
                        newState = TrackingState.Tracked;
                        break;

                    case TrackingState.Detected:
                        if (NumFramesInCurrentState >= numFramesDetectedUntilTracked)
                            newState = TrackingState.Tracked;
                        else
                            newState = TrackingState.Detected;
                        break;
                }
            }
            else if (CurrentState == TrackingState.Lost && NumFramesInCurrentState >= numFramesLostUntilDeleted)
                newState = TrackingState.Deleted;
            else
                newState = TrackingState.Lost;

            return newState;
        }
    }
}