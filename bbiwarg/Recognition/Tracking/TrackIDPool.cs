﻿using System.Collections.Generic;

namespace BBIWARG.Recognition.Tracking
{
    /// <summary>
    /// Generates unique IDs and keeps track of the currently used IDs.
    /// </summary>
    public class TrackIDPool
    {
        /// <summary>
        /// the currently used IDs
        /// </summary>
        private List<int> usedIDs;

        /// <summary>
        /// Initializes a new instance of the TrackIDPool class.
        /// </summary>
        public TrackIDPool()
        {
            usedIDs = new List<int>();
        }

        /// <summary>
        /// Returns the next unused (lowest) ID.
        /// </summary>
        /// <returns>the next unused ID</returns>
        public int getNextUnusedID()
        {
            int id = 1;
            while (usedIDs.Contains(id))
                id++;
            usedIDs.Add(id);
            return id;
        }

        /// <summary>
        /// Removes the given ID from the used IDs.
        /// </summary>
        /// <param name="id">the unused ID</param>
        public void setIDUnused(int id)
        {
            usedIDs.Remove(id);
        }
    }
}