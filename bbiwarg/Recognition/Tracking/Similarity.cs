﻿namespace BBIWARG.Recognition.Tracking
{
    /// <summary>
    /// Encapsulates the similarity between a TrackedObject and a TrackableObject.
    /// </summary>
    /// <typeparam name="T">Type of the TrackableObject</typeparam>
    /// <typeparam name="TrackedT">Type of the TrackedObject</typeparam>
    public class Similarity<T, TrackedT>
        where T : TrackableObject
        where TrackedT : TrackedObject<T>
    {
        /// <summary>
        /// the detected object
        /// </summary>
        public T DetectedObject { get; private set; }

        /// <summary>
        /// the tracked object
        /// </summary>
        public TrackedT TrackedObject { get; private set; }

        /// <summary>
        /// the similarity value [0-1]
        /// </summary>
        public float Value { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Similarity class.
        /// </summary>
        /// <param name="trackedObject">The tracked object.</param>
        /// <param name="detectedObject">The detected object.</param>
        /// <param name="value">The value.</param>
        public Similarity(TrackedT trackedObject, T detectedObject, float value)
        {
            TrackedObject = trackedObject;
            DetectedObject = detectedObject;
            Value = value;
        }
    }
}