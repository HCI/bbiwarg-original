﻿namespace BBIWARG.Recognition.TouchRecognition
{
    /// <summary>
    /// The type of the touch event.
    /// </summary>
    public enum TouchEventType
    {
        Down,
        Move,
        Up
    }

    /// <summary>
    /// Represents a touch event (type+touch)
    /// </summary>
    public class TouchEvent
    {
        /// <summary>
        /// the touch of the touch event
        /// </summary>
        public Touch Touch { get; private set; }

        /// <summary>
        /// the type of the touch event
        /// </summary>
        public TouchEventType Type { get; private set; }

        /// <summary>
        /// Initializes a new instance of the TouchEvent class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="touch">The touch.</param>
        public TouchEvent(TouchEventType type, Touch touch)
        {
            Type = type;
            Touch = touch;
        }
    }
}