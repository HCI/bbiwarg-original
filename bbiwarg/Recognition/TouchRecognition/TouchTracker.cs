﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using System.Collections.Generic;

namespace BBIWARG.Recognition.TouchRecognition
{
    /// <summary>
    /// Keeps track of touches over a period of time and generates touch events
    /// </summary>
    internal class TouchTracker : Tracker<Touch, TrackedTouch>
    {
        /// <summary>
        /// the touch events in the current frame
        /// </summary>
        private List<TouchEvent> touchEvents;

        /// <summary>
        /// Initializes a new instance of the TouchTracker class.
        /// </summary>
        /// <param name="imageSize">Size of the input image.</param>
        public TouchTracker(ImageSize imageSize)
            : base(imageSize)
        {
            touchEvents = new List<TouchEvent>();
        }

        /// <summary>
        /// Calculates the similarity [0-1] of a tracked touch and a detected touch.
        /// </summary>
        /// <param name="trackedTouch">the tracked touch</param>
        /// <param name="detectedTouch">the detected touch</param>
        /// <returns>the similarity</returns>
        public override float calculateSimilarity(TrackedTouch trackedTouch, Touch detectedTouch)
        {
            return (trackedTouch.FingerID == detectedTouch.Finger.TrackID) ? 1 : 0;
        }

        /// <summary>
        /// Gets all accumulated TouchEvents since the last flush.
        /// </summary>
        /// <returns>all accumulated touchEvents</returns>
        public List<TouchEvent> flushTouchEvents()
        {
            List<TouchEvent> flushedTouchEvents = touchEvents;
            touchEvents = new List<TouchEvent>();
            return flushedTouchEvents;
        }

        /// <summary>
        /// Updates the tracked touches, and stores the (optimized) touches in frameData.trackedTouches and the touch events in frameData.touchEvents.
        /// </summary>
        /// <param name="frameData">the current frame</param>
        public void trackTouches(FrameData frameData)
        {
            trackObjects(frameData.DetectedTouches);
            frameData.TrackedTouches = getOptimizedTouches();
            frameData.TouchEvents = flushTouchEvents();
        }

        /// <summary>
        /// Creates a new TrackedTouch
        /// </summary>
        /// <param name="detectedObject">the detected touch</param>
        /// <returns>a new TrackedTouch</returns>
        protected override TrackedTouch createTrackedObject(Touch detectedObject)
        {
            TrackedTouch tt = new TrackedTouch(idPool.getNextUnusedID(), detectedObject, Parameters.TouchTrackerNumFramesDetectedUntilTracked, Parameters.TouchTrackerNumFramesLostUntilDeleted);
            tt.TouchEvent += handleTouchEvent;
            return tt;
        }

        /// <summary>
        /// Get all optimized representations of all tracked touches
        /// </summary>
        /// <returns>all optimized tracked touches</returns>
        private List<Touch> getOptimizedTouches()
        {
            List<Touch> optimizedTouchs = new List<Touch>();
            foreach (TrackedTouch tp in TrackedObjects)
            {
                if (tp.IsTouchActive)
                    optimizedTouchs.Add(tp.OptimizedTouch);
            }
            return optimizedTouchs;
        }

        /// <summary>
        /// Handles a new TouchEvent
        /// </summary>
        /// <param name="sender">the sender of the event</param>
        /// <param name="e">the touch event</param>
        private void handleTouchEvent(object sender, TouchEvent e)
        {
            touchEvents.Add(e);
        }
    }
}