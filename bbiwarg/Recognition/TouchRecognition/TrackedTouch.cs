﻿using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using System;

namespace BBIWARG.Recognition.TouchRecognition
{
    /// <summary>
    /// signature of the touchEvent event
    /// </summary>
    /// <param name="sender">the event sender</param>
    /// <param name="e">the touch event</param>
    public delegate void TouchEventHandler(object sender, TouchEvent e);

    /// <summary>
    /// Represents a touch that is tracked for several frames
    /// </summary>
    public class TrackedTouch : TrackedObject<Touch>
    {
        /// <summary>
        /// the kalman filter for the absolute position prediction
        /// </summary>
        private Kalman2DPositionFilter absolutePositionKalman;

        /// <summary>
        /// the prediction of the absolute position
        /// </summary>
        public Vector2D AbsolutePositionPrediction { get { return absolutePositionKalman.getPrediction(); } }

        /// <summary>
        /// the track ID of the touching finger
        /// </summary>
        public int FingerID { get; private set; }

        /// <summary>
        /// indicates if the touch is currently active
        /// </summary>
        public bool IsTouchActive { get; private set; }

        /// <summary>
        /// the optimized touch (using the predicted absolute position)
        /// </summary>
        public Touch OptimizedTouch { get; private set; }

        /// <summary>
        /// the event which is fired, if a touchEvent occurs
        /// </summary>
        public event TouchEventHandler TouchEvent;

        /// <summary>
        /// Initializes a new instance of the TrackedTouch class.
        /// </summary>
        /// <param name="id">The track ID.</param>
        /// <param name="detectedTouch">The detected touch.</param>
        /// <param name="numFramesDetectedUntilTracked">The number of consecutive frames detected until the touch is considered to be tracked (touchDown).</param>
        /// <param name="numFramesLostUntilDeleted">The number of consecutive frames lost until the touch is deleted (touchUp).</param>
        public TrackedTouch(int id, Touch detectedTouch, int numFramesDetectedUntilTracked, int numFramesLostUntilDeleted)
            : base(id, detectedTouch, numFramesDetectedUntilTracked, numFramesLostUntilDeleted)
        {
            absolutePositionKalman = new Kalman2DPositionFilter(Parameters.TouchmXX, Parameters.TouchmXY, Parameters.TouchmYY);
            absolutePositionKalman.setInitialPosition(detectedTouch.AbsolutePosition);

            FingerID = detectedTouch.Finger.TrackID;
            IsTouchActive = false;

            logStateChange();
        }

        /// <summary>
        /// Updates the tracked touch, logs the state change, updates the optimized touch and the absolute position prediction and triggers touch events.
        /// </summary>
        /// <param name="detectedTouch">the detected touch</param>
        public override void updateFrame(Touch detectedTouch)
        {
            base.updateFrame(detectedTouch);

            if (NumFramesInCurrentState == 1)
                logStateChange();

            if (detectedTouch != null)
            {
                absolutePositionKalman.getCorrectedPosition(detectedTouch.AbsolutePosition);

                updateOptimizedTouch(detectedTouch);

                if (CurrentState == TrackingState.Tracked)
                {
                    if (!IsTouchActive)
                        TriggerTouchDown();
                    else
                        TriggerTouchMove();
                }
            }
            else if (IsTouchActive && CurrentState == TrackingState.Deleted)
                TriggerTouchUp();
        }

        /// <summary>
        /// logs the state change
        /// </summary>
        private void logStateChange()
        {
            String stateAsString = CurrentState.ToString().ToLower();
            Logger.log(String.Format("Touch #{0} {1}", this.ID, stateAsString), LogSubject.TouchTracker);
        }

        /// <summary>
        /// Fires a touch event with type = down
        /// </summary>
        private void TriggerTouchDown()
        {
            if (TouchEvent != null)
            {
                IsTouchActive = true;
                TouchEvent(this, new TouchEvent(TouchEventType.Down, OptimizedTouch));
            }
        }

        /// <summary>
        /// Fires a touch event with type = move
        /// </summary>
        private void TriggerTouchMove()
        {
            if (TouchEvent != null)
            {
                TouchEvent(this, new TouchEvent(TouchEventType.Move, OptimizedTouch));
            }
        }

        /// <summary>
        /// Fires a touch event with type = up
        /// </summary>
        private void TriggerTouchUp()
        {
            if (TouchEvent != null)
            {
                IsTouchActive = false;
                TouchEvent(this, new TouchEvent(TouchEventType.Up, OptimizedTouch));
            }
        }

        /// <summary>
        /// Updates the optimized touch using the absolute position prediction
        /// </summary>
        /// <param name="detectedTouch">the detected touch</param>
        private void updateOptimizedTouch(Touch detectedTouch)
        {
            OptimizedTouch = new Touch(AbsolutePositionPrediction, detectedTouch.Finger, detectedTouch.Palm);
            OptimizedTouch.setTracked(ID);
        }
    }
}