﻿using BBIWARG.Recognition.FingerRecognition;
using BBIWARG.Recognition.PalmRecognition;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;

namespace BBIWARG.Recognition.TouchRecognition
{
    /// <summary>
    /// Represents a touch (Finger touching a Palm)
    /// </summary>
    public class Touch : TrackableObject
    {
        /// <summary>
        /// the absolute position of the touch
        /// </summary>
        public Vector2D AbsolutePosition { get; private set; }

        /// <summary>
        /// the touching finger
        /// </summary>
        public Finger Finger { get; private set; }

        /// <summary>
        /// the touched palm
        /// </summary>
        public Palm Palm { get; private set; }

        /// <summary>
        /// the relative position of the touch within the palm quad
        /// </summary>
        public Vector2D RelativePosition { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Touch class.
        /// </summary>
        /// <param name="absolutePosition">The absolute position.</param>
        /// <param name="finger">The touching finger.</param>
        /// <param name="palm">The touched palm.</param>
        public Touch(Vector2D absolutePosition, Finger finger, Palm palm)
        {
            AbsolutePosition = absolutePosition;
            RelativePosition = palm.getRelativePosition(absolutePosition);
            Finger = finger;
            Palm = palm;

            finger.Touch = this;
        }
    }
}