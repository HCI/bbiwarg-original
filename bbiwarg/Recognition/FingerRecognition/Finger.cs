﻿using BBIWARG.Recognition.HandRecognition;
using BBIWARG.Recognition.TouchRecognition;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using Emgu.CV;
using System.Drawing;

namespace BBIWARG.Recognition.FingerRecognition
{
    /// <summary>
    /// The Finger class represents a Finger.
    /// </summary>
    public class Finger : TrackableObject
    {
        /// <summary>
        /// the direction of the finger (end to start)
        /// </summary>
        public Vector2D Direction { get { return SliceTrail.FittedDirection; } }

        /// <summary>
        /// the hand the finger belongs to
        /// </summary>
        public Hand Hand { get; set; }

        /// <summary>
        /// the position of the finger end (hand)
        /// </summary>
        public Vector2D HandPoint { get { return SliceTrail.EndSlice.Mid; } }

        /// <summary>
        /// the line segment from start to end
        /// </summary>
        public LineSegment2D LineSegment { get { return SliceTrail.LineSegment; } }

        /// <summary>
        /// a position in the middle of the finger
        /// </summary>
        public Vector2D MidPoint { get { return SliceTrail.MidSlice.Mid; } }

        /// <summary>
        /// the finger slices
        /// </summary>
        public FingerSliceTrail SliceTrail { get; private set; }

        /// <summary>
        /// the position of the finger tip
        /// </summary>
        public Vector2D TipPoint { get { return SliceTrail.StartSlice.Mid; } }

        /// <summary>
        /// the touch of the current finger
        /// </summary>
        public Touch Touch { get; set; }

        /// <summary>
        /// Initializes a new instance of the Finger class.
        /// </summary>
        /// <param name="sliceTrail">The finger slice trail.</param>
        public Finger(FingerSliceTrail sliceTrail)
            : base()
        {
            SliceTrail = sliceTrail;
        }

        /// <summary>
        /// Gets a contour of the finger
        /// </summary>
        /// <param name="margin">the margin around the finger (distance in pixels)</param>
        /// <returns>the contour of the finger</returns>
        public Contour<Point> getContour(float margin)
        {
            return SliceTrail.getContour(margin);
        }

        /// <summary>
        /// Reverses the finger (start to end)
        /// </summary>
        public void reverse()
        {
            SliceTrail.reverse();
        }
    }
}