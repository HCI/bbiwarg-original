﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;

namespace BBIWARG.Recognition.FingerRecognition
{
    /// <summary>
    /// Keeps track of fingers over a period of time.
    /// </summary>
    internal class FingerTracker : Tracker<Finger, TrackedFinger>
    {
        /// <summary>
        /// Initializes a new instance of the FingerTracker class.
        /// </summary>
        /// <param name="imageSize">Size of the input image.</param>
        public FingerTracker(ImageSize imageSize)
            : base(imageSize)
        {
        }

        /// <summary>
        /// Calculates the similarity [0-1] of a tracked Finger and a detected finger
        /// </summary>
        /// <param name="trackedFinger">the tracked finger</param>
        /// <param name="detectedFinger">the detected finger</param>
        /// <returns>the similarity between both fingers</returns>
        public override float calculateSimilarity(TrackedFinger trackedFinger, Finger detectedFinger)
        {
            Vector2D tip, hand;
            if (trackedFinger.shouldFingerBeReversed(detectedFinger))
            {
                tip = detectedFinger.HandPoint;
                hand = detectedFinger.TipPoint;
            }
            else
            {
                tip = detectedFinger.TipPoint;
                hand = detectedFinger.HandPoint;
            }

            float tipPointSimilarity = getPositionSimilarity(trackedFinger.TipPointPrediction, tip, Parameters.FingerTrackerMaxTipPointRelativeMove);
            float handPointSimilarity = getPositionSimilarity(trackedFinger.HandPointPrediction, hand, Parameters.FingerTrackerMaxHandPointRelativeMove);

            return tipPointSimilarity * handPointSimilarity;
        }

        /// <summary>
        /// Updates the TrackedFingers with the detected fingers in the current frame and stores the tracked fingers.
        /// </summary>
        /// <param name="frameData">the current frame</param>
        public void trackFingers(FrameData frameData)
        {
            trackObjects(frameData.DetectedFingers);
            frameData.TrackedFingers = getCurrentObjectsWithState(TrackingState.Tracked);
        }

        /// <summary>
        /// Creates a TrackedFinger.
        /// </summary>
        /// <param name="detectedObject">The detected finger.</param>
        /// <returns>a TrackedFinger</returns>
        protected override TrackedFinger createTrackedObject(Finger detectedObject)
        {
            return new TrackedFinger(idPool.getNextUnusedID(), detectedObject, Parameters.FingerTrackerNumFramesDetectedUntilTracked, Parameters.FingerTrackerNumFramesLostUntilDeleted);
        }
    }
}