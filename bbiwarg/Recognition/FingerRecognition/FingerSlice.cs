﻿using BBIWARG.Utility;

namespace BBIWARG.Recognition.FingerRecognition
{
    /// <summary>
    /// A Finger consists of multiple FingerSlices, each one represented by a line preferably orthogonal to the fingers direction.
    /// </summary>
    public class FingerSlice
    {
        /// <summary>
        /// the direction of the slice
        /// </summary>
        public Vector2D Direction { get { return LineSegment.Direction; } }

        /// <summary>
        /// the end point of the slice
        /// </summary>
        public Vector2D End { get; private set; }

        /// <summary>
        /// the length of the slice (in pixels)
        /// </summary>
        public float Length { get { return LineSegment.Length; } }

        /// <summary>
        /// the line segment connecting start and end
        /// </summary>
        public LineSegment2D LineSegment { get; private set; }

        /// <summary>
        /// the point in the middle of the slice
        /// </summary>
        public Vector2D Mid { get; private set; }

        /// <summary>
        /// the start point of the slice
        /// </summary>
        public Vector2D Start { get; private set; }

        /// <summary>
        /// Initializes a new instance of the FingerSlice class.
        /// </summary>
        /// <param name="start">The start point</param>
        /// <param name="end">The end point</param>
        public FingerSlice(Vector2D start, Vector2D end)
        {
            Start = start;
            End = end;
            Mid = (start + end) / 2;
            LineSegment = new LineSegment2D(Start, End);
        }
    }
}