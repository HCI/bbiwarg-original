﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;

namespace BBIWARG.Recognition.HandRecognition
{
    /// <summary>
    /// Keeps track of hands over a period of time.
    /// </summary>
    internal class HandTracker : Tracker<Hand, TrackedHand>
    {
        /// <summary>
        /// Initializes a new instance of the HandTracker class.
        /// </summary>
        /// <param name="imageSize">Size of the input image.</param>
        public HandTracker(ImageSize imageSize)
            : base(imageSize)
        {
        }

        /// <summary>
        /// Calculates the similarity [0-1] of a tracked Hand and a detected Hand.
        /// </summary>
        /// <param name="trackedHand">the tracked hand</param>
        /// <param name="detectedHand">the detected hand</param>
        /// <returns>the similarity</returns>
        public override float calculateSimilarity(TrackedHand trackedHand, Hand detectedHand)
        {
            return getPositionSimilarity(trackedHand.CentroidPrediction, detectedHand.Centroid, Parameters.HandTrackerMaxCentroidRelativeMove);
        }

        /// <summary>
        /// Updates the TrackedHands with the detected hands in the current frame and stores the results in frameData.trackedFingers.
        /// </summary>
        /// <param name="frameData">the current frame</param>
        public void trackHands(FrameData frameData)
        {
            trackObjects(frameData.DetectedHands);
            frameData.TrackedHands = getCurrentObjectsWithState(TrackingState.Tracked);
        }

        /// <summary>
        /// Creates a TrackedHand.
        /// </summary>
        /// <param name="detectedObject">the detected hand</param>
        /// <returns>a tracked hand</returns>
        protected override TrackedHand createTrackedObject(Hand detectedObject)
        {
            return new TrackedHand(idPool.getNextUnusedID(), detectedObject, Parameters.HandTrackerNumFramesDetectedUntilTracked, Parameters.HandTrackerNumFramesLostUntilDeleted);
        }
    }
}