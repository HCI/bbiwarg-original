﻿using BBIWARG.Recognition.Tracking;
using BBIWARG.Utility;
using System;

namespace BBIWARG.Recognition.HandRecognition
{
    /// <summary>
    /// Represents a hand that is tracked for several frames
    /// </summary>
    internal class TrackedHand : TrackedObject<Hand>
    {
        /// <summary>
        /// the kalman filter for the centroid prediction
        /// </summary>
        private Kalman2DPositionFilter centroidKalman;

        /// <summary>
        /// predicted position of the centroid
        /// </summary>
        public Vector2D CentroidPrediction { get { return centroidKalman.getPrediction(); } }

        /// <summary>
        /// Initializes a new instance of the TrackedHand class.
        /// </summary>
        /// <param name="id">The track ID.</param>
        /// <param name="detectedHand">The detected hand.</param>
        /// <param name="numFramesDetectedUntilTracked">The number of consecutive frames detected until the hand is considered to be tracked.</param>
        /// <param name="numFramesLostUntilDeleted">The number of consecutive frames lost until the hand should be deleted.</param>
        public TrackedHand(int id, Hand detectedHand, int numFramesDetectedUntilTracked, int numFramesLostUntilDeleted)
            : base(id, detectedHand, numFramesDetectedUntilTracked, numFramesLostUntilDeleted)
        {
            centroidKalman = new Kalman2DPositionFilter(Parameters.HandmXX, Parameters.HandmXY, Parameters.HandmYY);
            centroidKalman.setInitialPosition(detectedHand.Centroid);

            logStateChange();
        }

        /// <summary>
        /// Updates the tracked hand with the given hand, logs the state change and updates the centroid  prediction (kalman filter)
        /// </summary>
        /// <param name="detectedHand">the detected hand</param>
        public override void updateFrame(Hand detectedHand)
        {
            base.updateFrame(detectedHand);

            if (NumFramesInCurrentState == 1)
                logStateChange();

            if (detectedHand != null)
                centroidKalman.getCorrectedPosition(detectedHand.Centroid);
        }

        /// <summary>
        /// logs the state change
        /// </summary>
        private void logStateChange()
        {
            String stateAsString = CurrentState.ToString().ToLower();
            Logger.log(String.Format("Hand #{0} {1}", this.ID, stateAsString), LogSubject.HandTracker);
        }
    }
}