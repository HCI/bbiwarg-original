﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BBIWARG.Input.InputProviding
{
    class HackyInputProviderIntel : MarshalByRefObject, IInputProvider
    {
        private bool shouldRun;
        public int startId;

        public int CurrentFrameID
        {
            get;
            set;
        }

        public float FieldOfViewHorizontal
        {
            get;
            set;
        }

        public float FieldOfViewVertical
        {
            get;
            set;
        }

        public int ImageWidth
        {
            get { return 640; }
        }

        public int ImageHeight
        {
            get { return 480; }
        }

        public bool IsActive
        {
            get;
            set;
        }

        public UInt16 lowConfidenceValue;

        public event DeviceStartedEventHandler DeviceStartedEvent;
        public event NewFrameEventHandler NewFrameEvent;

        public void initialize()
        {
            CurrentFrameID = 0;
        }

        public void start()
        {
            shouldRun = true;
            if (DeviceStartedEvent != null)
            {
                DeviceStartedEvent(this, new EventArgs());
                DeviceStartedEvent = null; //only notify once...
            }

            IntelDomainWrapper wrapper = null;

            while (shouldRun) {
                if (wrapper == null || wrapper.errorstate) {//plus timestamp
                    //time to start a new one!
                    wrapper = new IntelDomainWrapper(this);
                    Task runCamTask = new Task(wrapper.run);

                    var timeoutCancellationTokenSource = new CancellationTokenSource();
                    
                }
            }
        }

        public void stop()
        {
            shouldRun = false;
        }

    }

    class IntelDomainWrapper {
        private AppDomain domain;
        private IntelCameraWrapper wrapper;

        public bool errorstate = false;
        private HackyInputProviderIntel inputProvider;

        public IntelDomainWrapper(HackyInputProviderIntel inputProvider) {
            this.inputProvider = inputProvider;
        }

        [HandleProcessCorruptedStateExceptions]
        public void run() {
            domain = System.AppDomain.CreateDomain("AppDomain-" + inputProvider.startId);
            inputProvider.startId++;

            wrapper = (IntelCameraWrapper)domain.CreateInstanceAndUnwrap(typeof(IntelCameraWrapper).Assembly.GetName().ToString(), typeof(IntelCameraWrapper).FullName);
            //wrapper.init(inputProvider);

            inputProvider.IsActive = true;

            try
            {
                wrapper.run();
            }
            catch (System.AccessViolationException)
            {
                Console.WriteLine("Camera caused corrupted process state.");
                errorstate = true;
            }


        }

    }
}
