﻿using Iisu;
using System;
using System.Threading;

namespace BBIWARG.Input.InputProviding
{
    /// <summary>
    /// signature for the event that the movie has restarted
    /// </summary>
    /// <param name="sender">event sender</param>
    /// <param name="e">event arguments</param>
    public delegate void MovieRestartedEventHandler(object sender, EventArgs e);

    /// <summary>
    /// VideoInputProvider provides the raw depth and confidence data read from a video file.
    /// </summary>
    internal class VideoInputProvider : InputProviderIisu
    {
        /// <summary>
        /// parameter handle for the current frame of the movie
        /// </summary>
        private IParameterHandle<int> currentMovieFrame;

        /// <summary>
        /// parameter handle for the number of frames in the movie
        /// </summary>
        private IParameterHandle<int> frameCount;

        /// <summary>
        /// parameter handle for the play step, which is the value that gets added to the current frame in each iteration
        /// </summary>
        private IParameterHandle<int> playStep;

        /// <summary>
        /// the id of the current frame
        /// </summary>
        public override int CurrentFrameID { get { return currentMovieFrame.Value; } }

        /// <summary>
        /// true iff the movie is paused
        /// </summary>
        public bool IsPaused { get; private set; }

        /// <summary>
        /// the path to the movie file
        /// </summary>
        public String MoviePath { get; private set; }

        /// <summary>
        /// event that the movie has restarted
        /// </summary>
        public event MovieRestartedEventHandler MovieRestartedEvent;

        /// <summary>
        /// Constructs a VideoInputProvider.
        /// </summary>
        /// <param name="moviePath">path to the movie file</param>
        public VideoInputProvider(String moviePath)
        {
            MoviePath = moviePath;
        }

        /// <summary>
        /// Jumps to the next movie frame.
        /// </summary>
        public void goToNextFrame()
        {
            nextFrame();
        }

        /// <summary>
        /// Jumps to the previous movie frame.
        /// </summary>
        public void goToPreviousFrame()
        {
            playStep.Value = -1;
            nextFrame();
            playStep.Value = 1;
        }

        /// <summary>
        /// Pauses the movie. No data is read anymore.
        /// </summary>
        public void pause()
        {
            IsPaused = true;
        }

        /// <summary>
        /// Resumes playing the movie and reading data.
        /// </summary>
        public void play()
        {
            IsPaused = false;
        }

        /// <summary>
        /// Returns an iisu device configuration, which uses the movie to read the data.
        /// </summary>
        /// <returns>iisu device configuration</returns>
        protected override IDeviceConfiguration createDeviceConfiguration()
        {
            IDeviceConfiguration conf = base.createDeviceConfiguration();
            conf.MoviePath = MoviePath;
            return conf;
        }

        /// <summary>
        /// Gets the next frame from the device and triggers an event if the movie has restarted.
        /// </summary>
        protected override void nextFrame()
        {
            base.nextFrame();

            if (CurrentFrameID == 0 && MovieRestartedEvent != null)
                MovieRestartedEvent(this, new EventArgs());
        }

        /// <summary>
        /// Registers all parameter and data handles.
        /// </summary>
        protected override void registerHandles()
        {
            base.registerHandles();

            device.RegisterParameterHandle<int>("SOURCE.MOVIE.PlayMode").Value = 1; // 0=once, 1=loop, 2=pingPong
            currentMovieFrame = device.RegisterParameterHandle<int>("SOURCE.MOVIE.CurrentFrame");
            frameCount = device.RegisterParameterHandle<int>("SOURCE.MOVIE.FrameCount");
            playStep = device.RegisterParameterHandle<int>("SOURCE.MOVIE.PlayStep");
        }

        /// <summary>
        /// Provides the main loop for reading data from the video file.
        /// </summary>
        protected override void run()
        {
            while (IsActive)
            {
                if (!IsPaused)
                    nextFrame();
                else
                    Thread.Sleep((int)(1000 / frameRate.Value));
            }
        }
    }
}