﻿using System;
using Emgu.CV;
using Emgu.CV.Structure;

namespace BBIWARG.Input.InputProviding
{
    /// <summary>
    /// Encapsulates the arguments of the event that a new frame is available.
    /// </summary>
    public class NewFrameEventArgs : EventArgs
    {
        /// <summary>
        /// the id of the frame
        /// </summary>
        public int FrameID { get; private set; }

        /// <summary>
        /// the height of all images in the frame
        /// </summary>
        public int Height { get { return DepthImageRaw.Height; } }

        public Image<Gray, UInt16> DepthImageRaw { get; private set; }

        /// <summary>
        /// the with of all images in the frame
        /// </summary>
        public int Width { get { return DepthImageRaw.Width; } }

        /// <summary>
        /// Constructs a NewFrameEventArgs.
        /// </summary>
        /// <param name="frameID">frame id</param>
        public NewFrameEventArgs(int frameID, Image<Gray, UInt16> depthImageRaw)
        {
            FrameID = frameID;
            DepthImageRaw = depthImageRaw;
        }
    }
}