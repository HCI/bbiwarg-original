﻿using System;


namespace BBIWARG.Input.InputProviding
{
    /// <summary>
    /// signature for the event that the device started
    /// </summary>
    /// <param name="sender">sender of the event</param>
    /// <param name="e">arguments of the event</param>
    public delegate void DeviceStartedEventHandler(object sender, EventArgs e);

    /// <summary>
    /// signature for the event that a new frame is available
    /// </summary>
    /// <param name="sender">sender of the event</param>
    /// <param name="e">arguments of the event</param>
    public delegate void NewFrameEventHandler(object sender, NewFrameEventArgs e);

    public interface IInputProvider
    {


        /// <summary>
        /// the id of the current frame
        /// </summary>
        int CurrentFrameID { get; }

        /// <summary>
        /// the horizontal field of view angle
        /// </summary>
        float FieldOfViewHorizontal { get; }

        /// <summary>
        /// the vertical field of view angle
        /// </summary>
        float FieldOfViewVertical { get; }

        /// <summary>
        /// the height of all images
        /// </summary>
        int ImageHeight { get; }

        /// <summary>
        /// the width of all images
        /// </summary>
        int ImageWidth { get; }

        /// <summary>
        /// true iff the input source provides data
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// event that the device started
        /// </summary>
        event DeviceStartedEventHandler DeviceStartedEvent;

        /// <summary>
        /// event that a new frame is available
        /// </summary>
        event NewFrameEventHandler NewFrameEvent;


        void initialize();
        void start();
        void stop();
    }
}