﻿using BBIWARG.Images;
using BBIWARG.Input.InputProviding;
using BBIWARG.Recognition.FingerRecognition;
using BBIWARG.Recognition.HandRecognition;
using BBIWARG.Recognition.PalmRecognition;
using BBIWARG.Recognition.TouchRecognition;
using BBIWARG.Utility;
using System;

namespace BBIWARG.Input.InputHandling
{
    /// <summary>
    /// signature for the event that a new frame is finished processing
    /// </summary>
    /// <param name="sender">sender of the event</param>
    /// <param name="e">event parameters</param>
    public delegate void NewProcessedFrameEventHandler(object sender, NewProcessedFrameEventArgs e);

    /// <summary>
    /// Handles new frames by coordinating and delegating the work to the image, detector and tracker classes.
    /// Also provides an event and the data for the tuio server and the graphical output.
    /// </summary>
    public class InputHandler
    {
        /// <summary>
        /// the finger detector
        /// </summary>
        private FingerDetector fingerDetector;

        /// <summary>
        /// the finger tracker
        /// </summary>
        private FingerTracker fingerTracker;

        /// <summary>
        /// the hand detector
        /// </summary>
        private HandDetector handDetector;

        /// <summary>
        /// the hand tracker
        /// </summary>
        private HandTracker handTracker;

        /// <summary>
        /// the input provider which provides the raw data
        /// </summary>
        private IInputProvider inputProvider;

        /// <summary>
        /// the palm detector
        /// </summary>
        private PalmDetector palmDetector;

        /// <summary>
        /// the palm tracker
        /// </summary>
        private PalmTracker palmTracker;

        /// <summary>
        /// set iff the source is a movie which is in the first frame again
        /// </summary>
        private bool resetFlag;

        /// <summary>
        /// the touch detector
        /// </summary>
        private TouchDetector touchDetector;

        /// <summary>
        /// the touch tracker
        /// </summary>
        private TouchTracker touchTracker;

        /// <summary>
        /// converts 2d and 3d coordinates into each other
        /// </summary>
        public CoordinateConverter CoordinateConverter { get; private set; }

        /// <summary>
        /// data for the current frame
        /// </summary>
        public FrameData FrameData { get; private set; }

        /// <summary>
        /// the size of all images
        /// </summary>
        public ImageSize ImageSize { get; private set; }

        /// <summary>
        /// event which occurs, when a new frame is finished processing
        /// </summary>
        public event NewProcessedFrameEventHandler NewProcessedFrameEvent;

        /// <summary>
        /// Constructs an InputHandler with an <see cref="InputProviderIisu"/>.
        /// </summary>
        /// <param name="inputProvider">the input provider</param>
        public InputHandler(IInputProvider inputProvider)
        {
            this.inputProvider = inputProvider;
            initialize();

            inputProvider.NewFrameEvent += handleNewFrame;
            VideoInputProvider videoInputProvider = inputProvider as VideoInputProvider;
            if (videoInputProvider != null)
                videoInputProvider.MovieRestartedEvent += handleMovieRestart;
        }

        /// <summary>
        /// Handles a new frame event by processing the new frame using the image, detector and tracker classes.
        /// </summary>
        /// <param name="sender">the sender of the event</param>
        /// <param name="e">the arguments for the new frame event</param>
        public void handleNewFrame(object sender, NewFrameEventArgs e)
        {
            Timer.start("InputHandler.handleNewFrame");

            FrameData frameData = new FrameData();

            frameData.FrameID = e.FrameID;
            frameData.ImageSize = ImageSize;

            // reset flag
            frameData.ResetFlag = resetFlag;
            resetFlag = false;

            // depth image
            Timer.start("InputHandler.handleNewFrame::createDepthImage");
            frameData.DepthImage = new DepthImage(e.DepthImageRaw);
            Timer.stop("InputHandler.handleNewFrame::createDepthImage");

            // edge image
            Timer.start("InputHandler.handleNewFrame::createEdgeImage");
            frameData.EdgeImage = new EdgeImage(frameData.DepthImage);
            Timer.stop("InputHandler.handleNewFrame::createEdgeImage");

            // detect fingers
            Timer.start("InputHandler.handleNewFrame::detectFingers");
            fingerDetector.detectFingers(frameData);
            Timer.stop("InputHandler.handleNewFrame::detectFingers");

            // track fingers
            Timer.start("InputHandler.handleNewFrame::trackFingers");
            fingerTracker.trackFingers(frameData);
            Timer.stop("InputHandler.handleNewFrame::trackFingers");

            // detect hands
            Timer.start("InputHandler.handleNewFrame::detectHands");
            handDetector.detectHands(frameData);
            Timer.stop("InputHandler.handleNewFrame::detectHands");

            // track hands
            Timer.start("InputHandler.handleNewFrame::trackHands");
            handTracker.trackHands(frameData);
            Timer.stop("InputHandler.handleNewFrame::trackHands");

            // detect palms
            Timer.start("InputHandler.handleNewFrame::detectPalms");
            palmDetector.detectPalms(frameData);
            Timer.stop("InputHandler.handleNewFrame::detectPalms");

            // track palms
            Timer.start("InputHandler.handleNewFrame::trackPalms");
            palmTracker.trackPalms(frameData);
            Timer.stop("InputHandler.handleNewFrame::trackPalms");

            // detect touches
            Timer.start("InputHandler.handleNewFrame::detectTouches");
            touchDetector.detectTouches(frameData);
            Timer.stop("InputHandler.handleNewFrame::detectTouches");

            // track touches
            Timer.start("InputHandler.handleNewFrame::trackTouches");
            touchTracker.trackTouches(frameData);
            Timer.stop("InputHandler.handleNewFrame::trackTouches");

            Timer.start("InputHandler.handleNewFrame::exportResults");
            FrameData = frameData;
            if (NewProcessedFrameEvent != null)
                NewProcessedFrameEvent(this, new NewProcessedFrameEventArgs(frameData));
            Timer.stop("InputHandler.handleNewFrame::exportResults");

            Timer.stop("InputHandler.handleNewFrame");

            if (Parameters.LoggerTimerOutputEnabled)
                Timer.outputAll();
        }

        /// <summary>
        /// Resets the trackers.
        /// </summary>
        public void reset()
        {
            touchTracker.reset();
            handTracker.reset();
            palmTracker.reset();
            fingerTracker.reset();
        }

        /// <summary>
        /// Handles the event of a restart of the movie.
        /// </summary>
        /// <param name="sender">the sender of the event</param>
        /// <param name="e">the event arguments</param>
        private void handleMovieRestart(object sender, EventArgs e)
        {
            reset();
            resetFlag = true;
        }

        /// <summary>
        /// Initializes all components.
        /// </summary>
        private void initialize()
        {
            ImageSize = new ImageSize(inputProvider.ImageWidth, inputProvider.ImageHeight);
            CoordinateConverter = new CoordinateConverter(inputProvider);
            resetFlag = false;

            fingerDetector = new FingerDetector(CoordinateConverter);
            handDetector = new HandDetector();
            palmDetector = new PalmDetector();
            touchDetector = new TouchDetector();

            fingerTracker = new FingerTracker(ImageSize);
            handTracker = new HandTracker(ImageSize);
            palmTracker = new PalmTracker(ImageSize);
            touchTracker = new TouchTracker(ImageSize);
        }
    }
}