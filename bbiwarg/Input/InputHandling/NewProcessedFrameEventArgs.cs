﻿using System;

namespace BBIWARG.Input.InputHandling
{
    /// <summary>
    /// Encapsulates the arguments for the event of finishing to process a new frame.
    /// </summary>
    public class NewProcessedFrameEventArgs : EventArgs
    {
        /// <summary>
        /// the data of the processed frame
        /// </summary>
        public FrameData FrameData { get; private set; }

        /// <summary>
        /// Creates a new NewProcessedFrameEventArgs with given frame data.
        /// </summary>
        /// <param name="frameData">frame data</param>
        public NewProcessedFrameEventArgs(FrameData frameData)
        {
            FrameData = frameData;
        }
    }
}