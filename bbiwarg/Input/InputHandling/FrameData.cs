﻿using BBIWARG.Images;
using BBIWARG.Recognition.FingerRecognition;
using BBIWARG.Recognition.HandRecognition;
using BBIWARG.Recognition.PalmRecognition;
using BBIWARG.Recognition.TouchRecognition;
using BBIWARG.Utility;
using System.Collections.Generic;
using Emgu.CV;
using Emgu.CV.Structure;

namespace BBIWARG.Input.InputHandling
{
    /// <summary>
    /// Data class which holds all data read an generated for one frame.
    /// </summary>
    public class FrameData
    {
        /// <summary>
        /// the depth image read in this frame
        /// </summary>
        public DepthImage DepthImage { get; set; }

        /// <summary>
        /// a list of fingers detected in this frame
        /// </summary>
        public List<Finger> DetectedFingers { get; set; }

        /// <summary>
        /// a list of hands detected in this frame
        /// </summary>
        public List<Hand> DetectedHands { get; set; }

        /// <summary>
        /// a list of palms detected in this frame
        /// </summary>
        public List<Palm> DetectedPalms { get; set; }

        /// <summary>
        /// a list of <see cref="Touch"/> objects detected in this frame
        /// </summary>
        public List<Touch> DetectedTouches { get; set; }

        /// <summary>
        /// the edge image created in this frame
        /// </summary>
        public EdgeImage EdgeImage { get; set; }

        /// <summary>
        /// the id of the frame
        /// </summary>
        public int FrameID { get; set; }

        /// <summary>
        /// the size of all the images
        /// </summary>
        public ImageSize ImageSize { get; set; }

        /// <summary>
        /// set iff the input source is a movie which is in the first frame again
        /// </summary>
        public bool ResetFlag { get; set; }

        /// <summary>
        /// a list of touch events generated in this frame
        /// </summary>
        public List<TouchEvent> TouchEvents { get; set; }

        /// <summary>
        /// a list of fingers which are tracked in this frame
        /// </summary>
        public List<Finger> TrackedFingers { get; set; }

        /// <summary>
        /// a list of hands which are tracked in this frame
        /// </summary>
        public List<Hand> TrackedHands { get; set; }

        /// <summary>
        /// a list of palms which are tracked in this frame
        /// </summary>
        public List<Palm> TrackedPalms { get; set; }

        /// <summary>
        /// a list of <see cref="Touch"/> objects which are tracked in this frame
        /// </summary>
        public List<Touch> TrackedTouches { get; set; }
    }
}