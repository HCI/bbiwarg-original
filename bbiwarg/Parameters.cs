﻿using BBIWARG.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace BBIWARG
{
    /// <summary>
    /// type of input source
    /// </summary>
    public enum InputType
    {
        RS300,
        DS325,
        Movie
    }

    /// <summary>
    /// Defines all parameters used in the whole program.
    /// </summary>
    internal static class Parameters
    {
        #region console

        /// <summary>
        /// height of the console in mono space characters
        /// </summary>
        public static readonly int ConsoleHeight = 30;

        /// <summary>
        /// with of the console in mono space characters
        /// </summary>
        public static readonly int ConsoleWidth = 90;

        #endregion console

        #region input

        /// <summary>
        /// path to the movie file used as input source
        /// </summary>
        public static readonly String InputMoviePath = "..\\..\\videos\\touch\\4.skv";

        /// <summary>
        /// the input source type
        /// </summary>
        public static readonly InputType InputSource = InputType.RS300;

        #endregion input

        #region Logger

        /// <summary>
        /// bit field which specifies which subjects should be logged
        /// </summary>
        public static readonly LogSubject LoggerEnabledSubjects = LogSubject.None;

        /// <summary>
        /// true iff the timer output should be shown
        /// </summary>
        public static readonly bool LoggerTimerOutputEnabled = false;

        #endregion Logger

        #region DebugWindow

        /// <summary>
        /// true iff the debug window is enabled
        /// </summary>
        public static readonly bool DebugWindowEnabled = true;

        /// <summary>
        /// the title of the debug window
        /// </summary>
        public static readonly String DebugWindowTitle = "BBIWARG - DebugOutput";

        /// <summary>
        /// the update interval for the debug window
        /// </summary>
        public static readonly int DebugWindowUpdateIntervall = 1000 / 30; // 30fps

        #endregion DebugWindow

        #region GlassesWindow

        /// <summary>
        /// true iff the glasses window is enabled
        /// </summary>
        public static readonly bool GlassesWindowEnabled = false;

        /// <summary>
        /// number of calibration points
        /// </summary>
        public static readonly int GlassesWindowNumCalibrationPoints = 20;

        /// <summary>
        /// the title of the debug window
        /// </summary>
        public static readonly String GlassesWindowTitle = "BBIWARG - GlassesOutput";

        /// <summary>
        /// the update interval for the glasses window
        /// </summary>
        public static readonly int GlassesWindowUpdateInterval = 1000 / 30; // 30fps

        #endregion GlassesWindow

        #region tuio

        /// <summary>
        /// the default ip address of the tuio server
        /// </summary>
        public static readonly String TuioDefaultIP = "127.0.0.1";

        /// <summary>
        /// the default port of the tuio server
        /// </summary>
        public static readonly Int16 TuioDefaultPort = 3336;

        /// <summary>
        /// true iff the tuio server is enabled
        /// </summary>
        public static readonly bool TuioEnabledByDefault = true;

        #endregion tuio

        #region ConfidenceImage

        /// <summary>
        /// the minimum confidence threshold for pixel values to be considered correct
        /// </summary>
        public static readonly int ConfidenceImageMinThreshold = 500;

        #endregion ConfidenceImage

        #region DepthImage

        /// <summary>
        /// the depth range which is considered important (in mm) (must be smaller than 255)
        /// </summary>
        public static readonly int DepthImageDepthRange = 254;

        /// <summary>
        /// the size of the median filter used to filter the depth image
        /// </summary>
        public static readonly int DepthImageMedianSize = 5;

        #endregion DepthImage

        #region EdgeImage

        /// <summary>
        /// linking threshold for the canny edge detector used to detect edges in the depth image
        /// </summary>
        public static readonly int EdgeImageCannyLinkingThreshold = 60;

        /// <summary>
        /// filter size for the canny edge detector used to detect edges in the depth image
        /// </summary>
        public static readonly int EdgeImageCannySize = 3;

        /// <summary>
        /// start threshold for the canny edge detector used to detect edges in the depth image
        /// </summary>
        public static readonly int EdgeImageCannyStartThreshold = 80;

        /// <summary>
        /// number of dilation iterations to generate the rough edge image from the edge image
        /// </summary>
        public static readonly int EdgeImageRoughNumDilationIterations = 1;

        #endregion EdgeImage

        #region general tracking

        /// <summary>
        /// if a tracked object moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float TrackerMaxRelativeMove = 0.35f;

        #endregion general tracking

        #region finger detection

        /// <summary>
        /// the contour margin around the finger
        /// </summary>
        public static readonly int FingerContourMargin = 4;

        /// <summary>
        /// the maximum depth difference between the finger and a point beside the finger
        /// </summary>
        public static readonly int FingerMaxCrippleDifference = 20;

        /// <summary>
        /// the number of missed slices until the trail expansion stops
        /// </summary>
        public static readonly int FingerMaxGapCounter = 4;

        /// <summary>
        /// the maximum slice length difference of two consecutive slices (used to drop outliers)
        /// </summary>
        public static int FingerMaxSliceLengthDifferencePerStep { get { return InputSource == InputType.RS300 ? 20 : 5; } }

        /// <summary>
        /// maximum finger slice length (in pixels)
        /// </summary>
        public static int FingerMaxWidth2D { get { return InputSource == InputType.RS300 ? 60 : 30; } }

        /// <summary>
        /// maximum finger slice length (in mm)
        /// </summary>
        public static readonly float FingerMaxWidth3D = 35.0f;

        /// <summary>
        /// the minimum number of slices a finger must have
        /// </summary>
        public static readonly int FingerMinNumSlices = 15;

        // TODO remove and replace with 3Dwidth

        /// <summary>
        /// minimum finger slice length (in pixels)
        /// </summary>
        public static readonly int FingerMinWidth2D = 2;

        /// <summary>
        /// the number of slices used to calculate the start and end directions
        /// </summary>
        public static readonly int FingerNumSlicesForRelativeDirection = 10;

        /// <summary>
        /// the distance of a point to be considered beside the finger (in pixels)
        /// </summary>
        public static readonly int FingerOutMargin = 6;

        /// <summary>
        /// the number of slices that are removed when the finger expansion starts in opposite direction (because initial slices don't have the correct direction)
        /// </summary>
        public static readonly int FingerRemoveNumSlicesForCorrection = 5;

        #endregion finger detection

        #region finger tracking

        /// <summary>
        /// xx entry for the measurement noise covariance matrix for the kalman filter used to smooth the finger hand and tip points
        /// </summary>
        public static readonly float FingermXX = 0.000005f;

        /// <summary>
        /// xy and yx entry for the measurement noise covariance matrix for the kalman filter used to smooth the finger hand and tip points
        /// </summary>
        public static readonly float FingermXY = 0.0f;

        /// <summary>
        /// yy entry for the measurement noise covariance matrix for the kalman filter used to smooth the finger hand and tip points
        /// </summary>
        public static readonly float FingermYY = 0.000005f;

        /// <summary>
        /// if the hand point of a finger moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float FingerTrackerMaxHandPointRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// if the tip point of a finger moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float FingerTrackerMaxTipPointRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// number of finger slice directions used to compute the mean finger direction
        /// </summary>
        public static readonly int FingerTrackerNumDirectionsForMeanDirection = 10;

        /// <summary>
        /// number of frames a finger needs to be detected before it is tracked
        /// </summary>
        public static readonly int FingerTrackerNumFramesDetectedUntilTracked = 5;

        /// <summary>
        /// number of frames a finger needs to be lost before it is deleted
        /// </summary>
        public static readonly int FingerTrackerNumFramesLostUntilDeleted = 10;

        #endregion finger tracking

        #region hand detection

        /// <summary>
        /// maximum depth difference below which two sections of a hand which are separated by a finger are considered to belong to the same hand
        /// </summary>
        public static readonly int HandExtendMaxDifference = 40;

        /// <summary>
        /// maximum size of a hand extension mask relative to the whole image
        /// </summary>
        public static readonly float HandExtensionMaxRelativeSize = 0.5f * HandMaxSize;

        /// <summary>
        /// maximum downwards depth difference between a pixel and a neighboring pixel belonging to the same hand
        /// </summary>
        public static int HandFloodFillDownDiff { get { return InputSource == InputType.RS300 ? 1 : 2;}}

        /// <summary>
        /// maximum upwards depth difference between a pixel and a neighboring pixel belonging to the same hand
        /// </summary>
        public static int HandFloodFillUpDiff { get { return InputSource == InputType.RS300 ? 1 : 2; } }

        /// <summary>
        /// maximum size of a hand relative to the whole image
        /// </summary>
        public static readonly float HandMaxSize = 0.6f;

        /// <summary>
        /// minimum size of a hand relative to the whole image
        /// </summary>
        public static readonly float HandMinSize = 0.01f;

        /// <summary>
        /// number of colors used to draw hands
        /// </summary>
        public static readonly int HandNumColors = 3;

        /// <summary>
        /// the maximum distance of the thumb tip point to the outer short point of a convexity defect for possible thumb defects
        /// </summary>
        public static readonly float HandThumbDefectMaxDistanceToThumb = Parameters.FingerMaxWidth2D;

        /// <summary>
        /// the maximum ratio of the length from the depth point to the outer short point to
        /// the length from the depth point to the outer long point of a convexity defect for possible thumb defects
        /// </summary>
        public static readonly float HandThumbDefectMaxShortLongLengthRatio = 0.7f;

        /// <summary>
        /// the maximum ratio of the thumb length to the length from the depth point to the outer short point of a convexity defect for possible thumb defects
        /// </summary>
        public static readonly float HandThumbDefectMaxThumbShortLengthRatio = 1.1f;

        /// <summary>
        /// the minimum ratio of the length from the depth point to the outer short point to
        /// the length from the depth point to the outer long point of a convexity defect for possible thumb defects
        /// </summary>
        public static readonly float HandThumbDefectMinShortLongLengthRatio = 0.3f;

        /// <summary>
        /// the minimum ratio of the thumb length to the length from the depth point to the outer short point of a convexity defect for possible thumb defects
        /// </summary>
        public static readonly float HandThumbDefectMinThumbShortLengthRatio = 0.6f;

        #endregion hand detection

        #region hand tracker

        /// <summary>
        /// xx entry for the measurement noise covariance matrix for the kalman filter used to smooth the hand centroid
        /// </summary>
        public static readonly float HandmXX = 0.0005f;

        /// <summary>
        /// xy and yx entry for the measurement noise covariance matrix for the kalman filter used to smooth the hand centroid
        /// </summary>
        public static readonly float HandmXY = 0.0f;

        /// <summary>
        /// yy entry for the measurement noise covariance matrix for the kalman filter used to smooth the hand centroid
        /// </summary>
        public static readonly float HandmYY = 0.0005f;

        /// <summary>
        /// if the centroid of a hand moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float HandTrackerMaxCentroidRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// number of frames a hand needs to be detected before it is tracked
        /// </summary>
        public static readonly int HandTrackerNumFramesDetectedUntilTracked = 5;

        /// <summary>
        /// number of frames a hand needs to be lost before it is deleted
        /// </summary>
        public static readonly int HandTrackerNumFramesLostUntilDeleted = 5;

        #endregion hand tracker

        #region palm detection

        /// <summary>
        /// relative tolerance which specifies when a point is considered to be in the palm grid
        /// </summary>
        public static readonly float PalmInsideTolerance = 0.1f;

        /// <summary>
        /// number of positions along the forefinger used as starting points to detect the palm width
        /// </summary>
        public static readonly int PalmNumPositionsForPalmWidth = 5;

        #endregion palm detection

        #region palm tracker

        /// <summary>
        /// xx entry for the measurement noise covariance matrix for the kalman filter used to smooth the palm grid points
        /// </summary>
        public static readonly float PalmmXX = 0.00005f;

        /// <summary>
        /// xy and yx entry for the measurement noise covariance matrix for the kalman filter used to smooth the palm grid points
        /// </summary>
        public static readonly float PalmmXY = 0.0f;

        /// <summary>
        /// yy entry for the measurement noise covariance matrix for the kalman filter used to smooth the palm grid points
        /// </summary>
        public static readonly float PalmmYY = 0.00005f;

        /// <summary>
        /// if the lower finger point of the palm grid moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float PalmTrackerMaxFingersLowerRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// if the upper finger point of the palm grid moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float PalmTrackerMaxFingersUpperRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// if the lower wrist point of the palm grid moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float PalmTrackerMaxWristLowerRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// if the upper wrist point of the palm grid moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float PalmTrackerMaxWristUpperRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// number of frames a palm needs to be detected before it is tracked
        /// </summary>
        public static readonly int PalmTrackerNumFramesDetectedUntilTracked = 5;

        /// <summary>
        /// number of frames a palm needs to be lost before it is deleted
        /// </summary>
        public static readonly int PalmTrackerNumFramesLostUntilDeleted = 5;

        #endregion palm tracker

        #region palm grid

        /// <summary>
        /// default number of palm grid columns
        /// </summary>
        public static readonly int PalmGridDefaultNumColumns = 4;

        /// <summary>
        /// default number of palm grid rows
        /// </summary>
        public static readonly int PalmGridDefaultNumRows = 3;

        /// <summary>
        /// number of palm slider capacity
        /// </summary>
        public static int PalmSliderMax = 10;

        /// <summary>
        /// position of palm slider
        /// </summary>
        public static int PalmSliderPos = 1;

        /// <summary>
        /// current value of palm slider
        /// </summary>
        public static int PalmSliderCurr = 0;

        /// <summary>
        /// current state of palm slider
        /// </summary>
        public static int PalmSliderState = 0;

        /// <summary>
        /// 
        /// </summary>
        public static int PalmSliderLastTouched = 0;


        #endregion palm grid

        #region touch detection

        /// <summary>
        /// the size of the quadrant around the touch positions that is used to determine the touch value
        /// </summary>
        public static int TouchAreaSize { get { return InputSource == InputType.RS300 ? 60 : 30; }}

        /// <summary>
        /// the maximum downwards difference for the flood fill operation
        /// </summary>
        public static readonly int TouchFloodfillDownDiff = 2;

        /// <summary>
        /// the maximum upwards difference for the flood fill operation
        /// </summary>
        public static readonly int TouchFloodfillUpDiff = 3;

        /// <summary>
        /// the threshold number of pixels affected by the flood fill (in percentage) to be considered as a touch
        /// </summary>
        public static readonly float TouchMinTouchValue = 0.5f;

        /// <summary>
        /// the position adjustment for the start point of the flood fill operation
        /// </summary>
        public static int TouchTipInsideFactor { get { return InputSource == InputType.RS300 ? 4 : 2; }}

        /// <summary>
        /// the position adjustment for the position of the actual touch event
        /// </summary>
        public static int TouchTipOutsideFactor { get { return InputSource == InputType.RS300 ? 14 : 7; }}

        #endregion touch detection

        #region touch tracking

        /// <summary>
        /// xx entry for the measurement noise covariance matrix for the kalman filter used to smooth touch events
        /// </summary>
        public static readonly float TouchmXX = 0.003f;

        /// <summary>
        /// xy and yx entry for the measurement noise covariance matrix for the kalman filter used to smooth touch events
        /// </summary>
        public static readonly float TouchmXY = 0.0f;

        /// <summary>
        /// yy entry for the measurement noise covariance matrix for the kalman filter used to smooth touch events
        /// </summary>
        public static readonly float TouchmYY = 0.003f;

        /// <summary>
        /// value used for all entries in the process noise covariance matrix for the kalman filter used to smooth touch events
        /// </summary>
        public static readonly float TouchProcessNoise = 3.0e-4f;

        /// <summary>
        /// if the absolute position of a tracked touch event moves this relative amount it will have a similarity of 0 to itself at the previous position
        /// </summary>
        public static readonly float TouchTrackerMaxAbsolutePositionRelativeMove = TrackerMaxRelativeMove;

        /// <summary>
        /// number of frames an object needs to be detected before it is tracked
        /// </summary>
        public static readonly int TouchTrackerNumFramesDetectedUntilTracked = 2;

        /// <summary>
        /// number of frames an object needs to be lost before it is deleted
        /// </summary>
        public static readonly int TouchTrackerNumFramesLostUntilDeleted = 10;

        #endregion touch tracking

        #region TouchEventVisualizer

        /// <summary>
        /// time in milliseconds after which old touch events are removed from the touch event visualizer
        /// </summary>
        public static readonly int TouchEventVisualizerFadeOutTime = 1500;

        #endregion TouchEventVisualizer

        #region homographyExport

        /// <summary>
        /// file name of the file to which the homography is written
        /// </summary>
        public static readonly String HomographyFileName = "homography.txt";

        #endregion homographyExport

        #region colors

        #region general

        /// <summary>
        /// color used to draw detected objects
        /// </summary>
        public static readonly Color ColorDetected = Color.Turquoise;

        /// <summary>
        /// color used to draw tracked objects
        /// </summary>
        public static readonly Color ColorTracked = Color.Yellow;

        #endregion general

        #region images

        /// <summary>
        /// color used to specify which color channels the depth image is drawn to
        /// </summary>
        public static readonly Color DepthImageColor = Color.White;

        /// <summary>
        /// color used to specify which color channels the edge image is drawn to
        /// </summary>
        public static readonly Color EdgeImageColor = Color.Blue;

        /// <summary>
        /// color used to draw the borders of the output images
        /// </summary>
        public static readonly Color OutputImageBorderColor = Color.White;

        #endregion images

        #region finger

        /// <summary>
        /// color used to draw the finger contour
        /// </summary>
        public static readonly Color FingerContourColor = Color.Red;

        /// <summary>
        /// color used to draw the detected fingers
        /// </summary>
        public static readonly Color FingerDetectedColor = ColorDetected;

        /// <summary>
        /// color used to draw the finger hand point
        /// </summary>
        public static readonly Color FingerHandColor = Color.Yellow;

        /// <summary>
        /// color used to draw the text for the finger id
        /// </summary>
        public static readonly Color FingerIDColor = Color.White;

        /// <summary>
        /// color used to draw the finger slices
        /// </summary>
        public static readonly Color FingerSliceColor = Color.Magenta;

        /// <summary>
        /// color used to draw the finger tip point
        /// </summary>
        public static readonly Color FingerTipColor = Color.Blue;

        /// <summary>
        /// color used to draw the tracked fingers
        /// </summary>
        public static readonly Color FingerTrackedColor = ColorTracked;

        #endregion finger

        #region touch

        /// <summary>
        /// color used to draw detected touch events
        /// </summary>
        public static readonly Color TouchEventDetectedColor = ColorDetected;

        /// <summary>
        /// color used to draw tracked touch events
        /// </summary>
        public static readonly Color TouchEventTrackedColor = ColorTracked;

        ///<summary>
        /// all touched buttons
        /// </summary>
        /// 
        public static List<List<bool>> ActiveTouches;

        #endregion touch

        #region TouchEventVisualizer

        /// <summary>
        /// color used to highlight the active block in the touch event visualizer
        /// </summary>
        public static readonly Color TouchEventVisualizerActiveBlockColor = Color.DarkSlateGray;

        /// <summary>
        /// color used to draw the grid in the touch event visualizer
        /// </summary>
        public static readonly Color TouchEventVisualizerGridColor = Color.White;

        /// <summary>
        /// color used to draw the lines between touch events in the touch event visualizer
        /// </summary>
        public static readonly Color TouchEventVisualizerLineColor = Color.Yellow;

        /// <summary>
        /// color used to draw the touch event points in the touch event visualizer
        /// </summary>
        public static readonly Color TouchEventVisualizerPointColor = Color.Red;

        /// <summary>
        /// color used to draw the text in the touch event visualizer
        /// </summary>
        public static readonly Color TouchEventVisualizerTextColor = Color.White;

        #endregion TouchEventVisualizer

        #region palm

        /// <summary>
        /// color used to draw the palm grid in the palm
        /// </summary>
        public static readonly Color PalmGridColor = Color.CornflowerBlue;

        /// <summary>
        /// color used to draw the palm quadrangle
        /// </summary>
        public static readonly Color PalmQuadColor = Color.Blue;

        #endregion palm

        #region hand

        /// <summary>
        /// color used to draw the hand centroid
        /// </summary>
        public static readonly Color HandCentroidColor = Color.Yellow;

        /// <summary>
        /// colors used to draw the hands (element is a color which specifies the color channels used to draw the hand)
        /// </summary>
        public static readonly Color[] HandColors = new Color[3] { Color.Red, Color.Blue, Color.Green };

        /// <summary>
        /// color used to draw the hand id text
        /// </summary>
        public static readonly Color HandIDColor = Color.White;

        /// <summary>
        /// color used to draw the lines of the thumb defects
        /// </summary>
        public static readonly Color HandThumbDefectLineColor = Color.CornflowerBlue;

        /// <summary>
        /// color used to draw the points of the thumb defects
        /// </summary>
        public static readonly Color HandThumbDefectPointColor = Color.Lime;

        #endregion hand

        #region calibration

        /// <summary>
        /// color used to draw the calibration points in the glasses window
        /// </summary>
        public static readonly Color CalibrationPointColor = Color.Yellow;

        #endregion calibration

        #endregion colors
    }
}