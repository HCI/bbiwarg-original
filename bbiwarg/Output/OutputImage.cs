﻿using BBIWARG.Utility;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace BBIWARG.Output
{
    /// <summary>
    /// An <see cref="Image"/> which provides additional drawing functions.
    /// </summary>
    internal class OutputImage : Image<Rgb, byte>
    {
        /// <summary>
        /// Creates an OutputImage.
        /// </summary>
        /// <param name="width">image width</param>
        /// <param name="height">image height</param>
        public OutputImage(int width, int height)
            : base(width, height)
        {
        }

        /// <summary>
        /// Creates an OutputImage.
        /// </summary>
        /// <param name="size">image size</param>
        public OutputImage(ImageSize size)
            : base(size.Width, size.Height)
        {
        }

        /// <summary>
        /// Draws a border around the image.
        /// </summary>
        /// <param name="color">color used to draw the border</param>
        public void drawBorder(Color color)
        {
            drawRectangle(new Rectangle(0, 0, Width, Height), color);
        }

        /// <summary>
        /// Draws a contour.
        /// </summary>
        /// <param name="contour">contour to draw</param>
        /// <param name="color">color used to draw the contour</param>
        /// <param name="thickness">thickness of the contour</param>
        public void drawContour(Contour<Point> contour, Color color, int thickness = 1)
        {
            Draw(contour, new Rgb(color), thickness);
        }

        /// <summary>
        /// Draws a convexity defect.
        /// </summary>
        /// <param name="defect">the defect to draw</param>
        /// <param name="pointColor">color used to draw the points of the defect</param>
        /// <param name="lineColor">color used to draw the lines of the defect</param>
        public void drawDefect(ConvexityDefect defect, Color pointColor, Color lineColor)
        {
            drawLineSegment(new Utility.LineSegment2D(defect.OuterShort, defect.Inner), lineColor);
            drawLineSegment(new Utility.LineSegment2D(defect.OuterLong, defect.Inner), lineColor);

            fillCircle(defect.Inner, 2, pointColor);
            fillCircle(defect.OuterShort, 2, pointColor);
            fillCircle(defect.OuterLong, 2, pointColor);
        }

        /// <summary>
        /// Draws a grayscale image into channels of the image.
        /// </summary>
        /// <param name="image">the image to draw</param>
        /// <param name="color">if a color component is != 0 the corresponding image channel is drawn to and image is scaled by (color component value / 255)</param>
        public void drawImage(Image<Gray, byte> image, Color color)
        {
            if (color.R != 0)
            {
                if (color.R != byte.MaxValue)
                    this[0] = this[0].Or(image.Mul((float)color.R / byte.MaxValue));
                else
                    this[0] = this[0].Or(image);
            }
            if (color.G != 0)
            {
                if (color.G != byte.MaxValue)
                    this[1] = this[1].Or(image.Mul((float)color.G / byte.MaxValue));
                else
                    this[1] = this[1].Or(image);
            }
            if (color.B != 0)
            {
                if (color.B != byte.MaxValue)
                    this[2] = this[2].Or(image.Mul((float)color.B / byte.MaxValue));
                else
                    this[2] = this[2].Or(image);
            }
        }

        /// <summary>
        /// Draws a line segment.
        /// </summary>
        /// <param name="lineSegment">line segment to draw</param>
        /// <param name="color">color used to draw the line segment</param>
        /// <param name="thickness">thickness of the line segment</param>
        public void drawLineSegment(Utility.LineSegment2D lineSegment, Color color, int thickness = 1)
        {
            Draw(new Emgu.CV.Structure.LineSegment2D(lineSegment.P1, lineSegment.P2), new Rgb(color), thickness);
        }

        /// <summary>
        /// Colors a pixel.
        /// </summary>
        /// <param name="position">position of the pixel</param>
        /// <param name="color">new color for pixel</param>
        public void drawPixel(Point position, Color color)
        {
            Data[position.Y, position.X, 0] = color.R;
            Data[position.Y, position.X, 0] = color.G;
            Data[position.Y, position.X, 0] = color.B;
        }

        /// <summary>
        /// Draws a grid into a quadrangle.
        /// </summary>
        /// <param name="quad">the quadrangle</param>
        /// <param name="borderColor">color used to draw the border lines</param>
        /// <param name="gridColor">color used to draw the grid</param>
        /// <param name="numRows">number of rows in the grid</param>
        /// <param name="numCols">number of columns in the grid</param>
        public void drawQuadrangleGrid(Quadrangle quad, Color borderColor, Color gridColor, int numRows, int numCols)
        {
            Vector2D a = quad.TopLeft;
            Vector2D b = quad.TopRight;
            Vector2D c = quad.BottomRight;
            Vector2D d = quad.BottomLeft;

            Vector2D relAB = (b - a) / numCols;
            Vector2D relDC = (c - d) / numCols;
            Vector2D relBC = (c - b) / numRows;
            Vector2D relAD = (d - a) / numRows;

            for (int i = 1; i < numCols; i++)
            {
                drawLineSegment(new Utility.LineSegment2D(a + i * relAB, d + i * relDC), gridColor);
            }

            for (int i = 1; i < numRows; i++)
            {
                drawLineSegment(new Utility.LineSegment2D(a + i * relAD, b + i * relBC), gridColor);
            }

            drawLineSegment(new Utility.LineSegment2D(a, b), borderColor);
            drawLineSegment(new Utility.LineSegment2D(b, c), borderColor);
            drawLineSegment(new Utility.LineSegment2D(c, d), borderColor);
            drawLineSegment(new Utility.LineSegment2D(d, a), borderColor);
        }

        /// <summary>
        /// Draws a unfilled rectangle.
        /// </summary>
        /// <param name="rect">the rectangle</param>
        /// <param name="color">color used to raw the rectangle</param>
        /// <param name="thichness">thickness of the rectangle</param>
        public void drawRectangle(Rectangle rect, Color color, int thichness = 0)
        {
            Draw(rect, new Rgb(color), thichness);
        }

        /// <summary>
        /// Draws text.
        /// </summary>
        /// <param name="position">top left corner to draw the text at</param>
        /// <param name="text">the text to draw</param>
        /// <param name="color">color used to draw the text</param>
        public void drawText(Point position, String text, Color color)
        {
            MCvFont font = new MCvFont(Emgu.CV.CvEnum.FONT.CV_FONT_HERSHEY_PLAIN, 1, 1);
            Draw(text, ref font, position, new Rgb(color));
        }

        /// <summary>
        /// Draws a touch gesture as the current point and a faded line.
        /// </summary>
        /// <param name="positions">touch positions</param>
        /// <param name="scale">scale used to transform given positions into positions in the image</param>
        /// <param name="opacity">opacity of the faded lines</param>
        public void drawTouchGesture(List<Vector2D> positions, Vector2D scale, float opacity = 1)
        {
            int numPositions = positions.Count;
            Color lineColor = Parameters.TouchEventVisualizerLineColor;
            Color pointColor = Parameters.TouchEventVisualizerPointColor;
            Color lineColorFaded = Color.FromArgb((int)(opacity * lineColor.R), (int)(opacity * lineColor.G), (int)(opacity * lineColor.B));
            Color pointColorFaded = Color.FromArgb((int)(opacity * pointColor.R), (int)(opacity * pointColor.G), (int)(opacity * pointColor.B));
            for (int i = 1; i < numPositions; i++)
            {
                drawLineSegment(new Utility.LineSegment2D(positions[i - 1].scale(scale), positions[i].scale(scale)), lineColorFaded);
            }
            Vector2D lastPos = positions[numPositions - 1].scale(scale);
            fillCircle(lastPos, 3, pointColorFaded);
        }

        /// <summary>
        /// Draws a filled circle.
        /// </summary>
        /// <param name="position">center of the circle</param>
        /// <param name="radius">radius of the circle</param>
        /// <param name="color">color used to draw the circle</param>
        public void fillCircle(Point position, float radius, Color color)
        {
            Draw(new CircleF(position, radius), new Rgb(color), 0);
        }

        /// <summary>
        /// Draws a filled rectangle.
        /// </summary>
        /// <param name="rect">the rectangle</param>
        /// <param name="color">color used to fill the rectangle</param>
        public void fillRectangle(Rectangle rect, Color color)
        {
            Draw(rect, new Rgb(color), -1);
        }

        /// <summary>
        /// Returns the color at a position.
        /// </summary>
        /// <param name="x">x coordinate of the position</param>
        /// <param name="y">y coordinate of the position</param>
        /// <returns>color at position (x, y)</returns>
        public Color getColorAt(int x, int y)
        {
            byte red = Data[y, x, 0];
            byte green = Data[y, x, 1];
            byte blue = Data[y, x, 2];
            return Color.FromArgb(red, green, blue);
        }
    }
}