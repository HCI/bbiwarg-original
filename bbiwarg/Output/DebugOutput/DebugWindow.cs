﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Input.InputProviding;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace BBIWARG.Output.DebugOutput
{
    /// <summary>
    /// A Windows Form which shows the debug images.
    /// </summary>
    public partial class DebugWindow : Form
    {
        /// <summary>
        /// id of the current frame
        /// </summary>
        private int currentFrameID;

        private API.Grid grid;

        /// <summary>
        /// the debug image creator
        /// </summary>
        private DebugImageCreator debugImageCreator;

        /// <summary>
        /// true iff the window is showing the latest data
        /// </summary>
        private bool guiUpToDate;

        /// <summary>
        /// the input handler
        /// </summary>
        private InputHandler inputHandler;

        /// <summary>
        /// the input provider
        /// </summary>
        private IInputProvider inputProvider;

        /// <summary>
        /// timer to periodically update the window
        /// </summary>
        private System.Windows.Forms.Timer timer;

        /// <summary>
        /// Creates the DebugWindow.
        /// </summary>
        /// <param name="inputProvider">input provider</param>
        /// <param name="inputHandler">input handle</param>
        /// <param name="name">the title of the window</param>
        /// <param name="updateInterval">the update interval for the window in milliseconds</param>
        public DebugWindow(IInputProvider inputProvider, InputHandler inputHandler, String name, int updateInterval)
        {
            InitializeComponent();

            this.inputProvider = inputProvider;
            this.inputHandler = inputHandler;
            guiUpToDate = false;

            Name = name;
            Text = name;

            TouchEventVisualizer touchEventVisualizer = new TouchEventVisualizer();
            inputHandler.NewProcessedFrameEvent += touchEventVisualizer.handleNewFrameData;
            debugImageCreator = new DebugImageCreator(touchEventVisualizer);

            if (inputProvider is VideoInputProvider)
                playPauseButton.Enabled = true;

            palmGridNumRowsTrackBar.Value = Parameters.PalmGridDefaultNumRows;
            palmGridNumColumnsTrackBar.Value = Parameters.PalmGridDefaultNumColumns;

            grid = API.Grid.getInctance();
            grid.Cols = Parameters.PalmGridDefaultNumColumns;
            grid.Rows = Parameters.PalmGridDefaultNumRows;

            timer = new System.Windows.Forms.Timer();
            timer.Interval = updateInterval;
            timer.Tick += update;
            timer.Start();
        }

        /// <summary>
        /// Stops the input provider when closing the window.
        /// </summary>
        /// <param name="e">event arguments</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            inputProvider.stop();
        }

        /// <summary>
        /// Jumps to the next movie frame.
        /// </summary>
        private void handleGoToNextFrame()
        {
            VideoInputProvider videoInputProvider = inputProvider as VideoInputProvider;
            videoInputProvider.goToNextFrame();
        }

        /// <summary>
        /// Jumps to the previous movie frame.
        /// </summary>
        private void handleGoToPreviousFrame()
        {
            VideoInputProvider videoInputProvider = inputProvider as VideoInputProvider;
            videoInputProvider.goToPreviousFrame();
        }

        /// <summary>
        /// Toggles the paused state of the movie.
        /// </summary>
        private void handlePlayPause()
        {
            VideoInputProvider videoInputProvider = inputProvider as VideoInputProvider;

            if (videoInputProvider.IsPaused)
            {
                videoInputProvider.play();
                playPauseButton.Text = "Pause";
                nextFrameButton.Enabled = false;
                previousFrameButton.Enabled = false;
            }
            else
            {
                videoInputProvider.pause();
                playPauseButton.Text = "Play";
                nextFrameButton.Enabled = true;
                previousFrameButton.Enabled = true;
            }
        }

        /// <summary>
        /// Handles the click on the next frame button.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private void nextFrameButton_Click(object sender, EventArgs e)
        {
            handleGoToNextFrame();
        }

        /// <summary>
        /// Updates the window when the number of rows or columns in the palm grid are changed.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private void palmGridTrackBar_Scroll(object sender, EventArgs e)
        {
            currentFrameID = -1;
            update(sender, e);
        }

        /// <summary>
        /// Handles the click on the play / pause button.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private void playPauseButton_Click(object sender, EventArgs e)
        {
            handlePlayPause();
        }

        /// <summary>
        /// Handles the click on the previous frame button.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private void previousFrameButton_Click(object sender, EventArgs e)
        {
            handleGoToPreviousFrame();
        }

        /// <summary>
        /// Updates the window.
        /// </summary>
        /// <param name="sender">the event sender</param>
        /// <param name="e">the event arguments</param>
        private void update(object sender, EventArgs e)
        {
            Utility.Timer.start("DebugWindow.update");

            if (!inputProvider.IsActive)
                Close();

            FrameData frameData = inputHandler.FrameData;
            if (frameData != null)
            {
                lock (frameData)
                {
                    if (currentFrameID != frameData.FrameID)
                    {
                        currentFrameID = frameData.FrameID;
                        Utility.Timer.start("DebugWindow.update::updateImages");
                        updateImages(frameData);
                        Utility.Timer.stop("DebugWindow.update::updateImages");
                    }
                }
            }

            if (!guiUpToDate)
            {
                Utility.Timer.start("DebugWindow.update::updateGUI");
                updateGUI();
                Utility.Timer.stop("DebugWindow.update::updateGUI");
            }

            Utility.Timer.stop("DebugWindow.update");
        }

        /// <summary>
        /// Updates the GUI elements.
        /// </summary>
        private void updateGUI()
        {
            // update image boxes
            depthImageBox.Image = debugImageCreator.DepthImage;
            fingerImageBox.Image = debugImageCreator.FingerImage;
            handImageBox.Image = debugImageCreator.HandImage;
            palmImageBox.Image = debugImageCreator.PalmImage;
            touchImageBox.Image = debugImageCreator.TouchImage;

            // update frame label
            frameLabel.Text = "Frame: " + currentFrameID;

            guiUpToDate = true;
        }

        /// <summary>
        /// Updates the debug images.
        /// </summary>
        /// <param name="frameData">data for the new frame</param>
        private void updateImages(FrameData frameData)
        {
            guiUpToDate = false;

            int numRows = palmGridNumRowsTrackBar.Value;
            int numColumns = palmGridNumColumnsTrackBar.Value;

            debugImageCreator.updateImages(frameData, numRows, numColumns);
        }
    }
}