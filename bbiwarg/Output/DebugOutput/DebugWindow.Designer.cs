﻿namespace BBIWARG.Output.DebugOutput
{
    partial class DebugWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.depthImageBox = new Emgu.CV.UI.ImageBox();
            this.fingerImageBox = new Emgu.CV.UI.ImageBox();
            this.handImageBox = new Emgu.CV.UI.ImageBox();
            this.palmImageBox = new Emgu.CV.UI.ImageBox();
            this.imageBox6 = new Emgu.CV.UI.ImageBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.frameLabel = new System.Windows.Forms.Label();
            this.palmGridNumColumnsTrackBar = new System.Windows.Forms.TrackBar();
            this.palmGridNumColumnsLabel = new System.Windows.Forms.Label();
            this.palmGridNumRowsLabel = new System.Windows.Forms.Label();
            this.palmGridLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.videoControlLabel = new System.Windows.Forms.Label();
            this.palmGridNumRowsTrackBar = new System.Windows.Forms.TrackBar();
            this.previousFrameButton = new System.Windows.Forms.Button();
            this.nextFrameButton = new System.Windows.Forms.Button();
            this.playPauseButton = new System.Windows.Forms.Button();
            this.touchImageBox = new Emgu.CV.UI.ImageBox();
            ((System.ComponentModel.ISupportInitialize)(this.depthImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fingerImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.handImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palmImageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palmGridNumColumnsTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palmGridNumRowsTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.touchImageBox)).BeginInit();
            this.SuspendLayout();
            // 
            // depthImageBox
            // 
            this.depthImageBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.depthImageBox.Location = new System.Drawing.Point(0, 0);
            this.depthImageBox.Margin = new System.Windows.Forms.Padding(0);
            this.depthImageBox.Name = "depthImageBox";
            this.depthImageBox.Size = new System.Drawing.Size(320, 240);
            this.depthImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.depthImageBox.TabIndex = 2;
            this.depthImageBox.TabStop = false;
            // 
            // fingerImageBox
            // 
            this.fingerImageBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.fingerImageBox.Location = new System.Drawing.Point(320, 0);
            this.fingerImageBox.Margin = new System.Windows.Forms.Padding(0);
            this.fingerImageBox.Name = "fingerImageBox";
            this.fingerImageBox.Size = new System.Drawing.Size(320, 240);
            this.fingerImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fingerImageBox.TabIndex = 2;
            this.fingerImageBox.TabStop = false;
            // 
            // handImageBox
            // 
            this.handImageBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.handImageBox.Location = new System.Drawing.Point(640, 0);
            this.handImageBox.Margin = new System.Windows.Forms.Padding(0);
            this.handImageBox.Name = "handImageBox";
            this.handImageBox.Size = new System.Drawing.Size(320, 240);
            this.handImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.handImageBox.TabIndex = 2;
            this.handImageBox.TabStop = false;
            // 
            // palmImageBox
            // 
            this.palmImageBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.palmImageBox.Location = new System.Drawing.Point(0, 240);
            this.palmImageBox.Margin = new System.Windows.Forms.Padding(0);
            this.palmImageBox.Name = "palmImageBox";
            this.palmImageBox.Size = new System.Drawing.Size(320, 240);
            this.palmImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.palmImageBox.TabIndex = 2;
            this.palmImageBox.TabStop = false;
            // 
            // imageBox6
            // 
            this.imageBox6.Location = new System.Drawing.Point(574, 229);
            this.imageBox6.Margin = new System.Windows.Forms.Padding(0);
            this.imageBox6.Name = "imageBox6";
            this.imageBox6.Size = new System.Drawing.Size(0, 0);
            this.imageBox6.TabIndex = 2;
            this.imageBox6.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.frameLabel);
            this.panel1.Controls.Add(this.palmGridNumColumnsTrackBar);
            this.panel1.Controls.Add(this.palmGridNumColumnsLabel);
            this.panel1.Controls.Add(this.palmGridNumRowsLabel);
            this.panel1.Controls.Add(this.palmGridLabel);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.videoControlLabel);
            this.panel1.Controls.Add(this.palmGridNumRowsTrackBar);
            this.panel1.Controls.Add(this.previousFrameButton);
            this.panel1.Controls.Add(this.nextFrameButton);
            this.panel1.Controls.Add(this.playPauseButton);
            this.panel1.Location = new System.Drawing.Point(640, 240);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(320, 240);
            this.panel1.TabIndex = 3;
            // 
            // frameLabel
            // 
            this.frameLabel.Location = new System.Drawing.Point(116, 14);
            this.frameLabel.Name = "frameLabel";
            this.frameLabel.Size = new System.Drawing.Size(77, 13);
            this.frameLabel.TabIndex = 12;
            this.frameLabel.Text = "Frame:";
            this.frameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // palmGridNumColumnsTrackBar
            // 
            this.palmGridNumColumnsTrackBar.Location = new System.Drawing.Point(56, 148);
            this.palmGridNumColumnsTrackBar.Minimum = 1;
            this.palmGridNumColumnsTrackBar.Name = "palmGridNumColumnsTrackBar";
            this.palmGridNumColumnsTrackBar.Size = new System.Drawing.Size(247, 45);
            this.palmGridNumColumnsTrackBar.TabIndex = 4;
            this.palmGridNumColumnsTrackBar.Value = 1;
            this.palmGridNumColumnsTrackBar.Scroll += new System.EventHandler(this.palmGridTrackBar_Scroll);
            // 
            // palmGridNumColumnsLabel
            // 
            this.palmGridNumColumnsLabel.AutoSize = true;
            this.palmGridNumColumnsLabel.Location = new System.Drawing.Point(3, 148);
            this.palmGridNumColumnsLabel.Name = "palmGridNumColumnsLabel";
            this.palmGridNumColumnsLabel.Size = new System.Drawing.Size(47, 13);
            this.palmGridNumColumnsLabel.TabIndex = 10;
            this.palmGridNumColumnsLabel.Text = "Columns";
            // 
            // palmGridNumRowsLabel
            // 
            this.palmGridNumRowsLabel.AutoSize = true;
            this.palmGridNumRowsLabel.Location = new System.Drawing.Point(3, 108);
            this.palmGridNumRowsLabel.Name = "palmGridNumRowsLabel";
            this.palmGridNumRowsLabel.Size = new System.Drawing.Size(34, 13);
            this.palmGridNumRowsLabel.TabIndex = 9;
            this.palmGridNumRowsLabel.Text = "Rows";
            // 
            // palmGridLabel
            // 
            this.palmGridLabel.AutoSize = true;
            this.palmGridLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.palmGridLabel.Location = new System.Drawing.Point(3, 83);
            this.palmGridLabel.Name = "palmGridLabel";
            this.palmGridLabel.Size = new System.Drawing.Size(61, 13);
            this.palmGridLabel.TabIndex = 8;
            this.palmGridLabel.Text = "Palm Grid";
            // 
            // label2
            // 
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label2.Location = new System.Drawing.Point(98, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 0);
            this.label2.TabIndex = 7;
            // 
            // videoControlLabel
            // 
            this.videoControlLabel.AutoSize = true;
            this.videoControlLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.videoControlLabel.Location = new System.Drawing.Point(3, 14);
            this.videoControlLabel.Name = "videoControlLabel";
            this.videoControlLabel.Size = new System.Drawing.Size(89, 13);
            this.videoControlLabel.TabIndex = 6;
            this.videoControlLabel.Text = "Video Controls";
            // 
            // palmGridNumRowsTrackBar
            // 
            this.palmGridNumRowsTrackBar.Location = new System.Drawing.Point(56, 108);
            this.palmGridNumRowsTrackBar.Minimum = 1;
            this.palmGridNumRowsTrackBar.Name = "palmGridNumRowsTrackBar";
            this.palmGridNumRowsTrackBar.Size = new System.Drawing.Size(247, 45);
            this.palmGridNumRowsTrackBar.TabIndex = 3;
            this.palmGridNumRowsTrackBar.Value = 1;
            this.palmGridNumRowsTrackBar.Scroll += new System.EventHandler(this.palmGridTrackBar_Scroll);
            // 
            // previousFrameButton
            // 
            this.previousFrameButton.Enabled = false;
            this.previousFrameButton.Location = new System.Drawing.Point(6, 35);
            this.previousFrameButton.Name = "previousFrameButton";
            this.previousFrameButton.Size = new System.Drawing.Size(104, 23);
            this.previousFrameButton.TabIndex = 0;
            this.previousFrameButton.Text = "previousFrame";
            this.previousFrameButton.UseVisualStyleBackColor = true;
            this.previousFrameButton.Click += new System.EventHandler(this.previousFrameButton_Click);
            // 
            // nextFrameButton
            // 
            this.nextFrameButton.Enabled = false;
            this.nextFrameButton.Location = new System.Drawing.Point(199, 35);
            this.nextFrameButton.Name = "nextFrameButton";
            this.nextFrameButton.Size = new System.Drawing.Size(104, 23);
            this.nextFrameButton.TabIndex = 2;
            this.nextFrameButton.Text = "nextFrame";
            this.nextFrameButton.UseVisualStyleBackColor = true;
            this.nextFrameButton.Click += new System.EventHandler(this.nextFrameButton_Click);
            // 
            // playPauseButton
            // 
            this.playPauseButton.Enabled = false;
            this.playPauseButton.Location = new System.Drawing.Point(116, 35);
            this.playPauseButton.Name = "playPauseButton";
            this.playPauseButton.Size = new System.Drawing.Size(77, 23);
            this.playPauseButton.TabIndex = 1;
            this.playPauseButton.Text = "Pause";
            this.playPauseButton.UseVisualStyleBackColor = true;
            this.playPauseButton.Click += new System.EventHandler(this.playPauseButton_Click);
            // 
            // touchImageBox
            // 
            this.touchImageBox.FunctionalMode = Emgu.CV.UI.ImageBox.FunctionalModeOption.Minimum;
            this.touchImageBox.Location = new System.Drawing.Point(320, 240);
            this.touchImageBox.Margin = new System.Windows.Forms.Padding(0);
            this.touchImageBox.Name = "touchImageBox";
            this.touchImageBox.Size = new System.Drawing.Size(320, 240);
            this.touchImageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.touchImageBox.TabIndex = 2;
            this.touchImageBox.TabStop = false;
            // 
            // DebugWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1108, 578);
            this.Controls.Add(this.touchImageBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.imageBox6);
            this.Controls.Add(this.palmImageBox);
            this.Controls.Add(this.handImageBox);
            this.Controls.Add(this.fingerImageBox);
            this.Controls.Add(this.depthImageBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DebugWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DebugWindow";
            ((System.ComponentModel.ISupportInitialize)(this.depthImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fingerImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.handImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palmImageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox6)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.palmGridNumColumnsTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palmGridNumRowsTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.touchImageBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Emgu.CV.UI.ImageBox depthImageBox;
        private Emgu.CV.UI.ImageBox fingerImageBox;
        private Emgu.CV.UI.ImageBox handImageBox;
        private Emgu.CV.UI.ImageBox palmImageBox;
        private Emgu.CV.UI.ImageBox imageBox6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button playPauseButton;
        private System.Windows.Forms.Label videoControlLabel;
        private System.Windows.Forms.Button previousFrameButton;
        private System.Windows.Forms.Button nextFrameButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label palmGridLabel;
        private System.Windows.Forms.Label palmGridNumRowsLabel;
        private System.Windows.Forms.Label palmGridNumColumnsLabel;
        private System.Windows.Forms.Label frameLabel;
        private Emgu.CV.UI.ImageBox touchImageBox;
        public System.Windows.Forms.TrackBar palmGridNumRowsTrackBar;
        public System.Windows.Forms.TrackBar palmGridNumColumnsTrackBar;
    }
}