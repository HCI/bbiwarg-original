﻿using BBIWARG.Input.InputHandling;
using BBIWARG.Input.InputProviding;
using BBIWARG.Output.DebugOutput;
using BBIWARG.Output.GlassesOutput;
using BBIWARG.TUIO;
using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace BBIWARG
{
    /// <summary>
    /// Main class which creates and starts the input, tuio and window objects.
    /// </summary>
    internal class BBIWARG
    {
        /// <summary>
        /// the debug window
        /// </summary>
        private DebugWindow debugWindow;

        /// <summary>
        /// true iff the debug window is enabled
        /// </summary>
        private bool debugWindowEnabled;

        /// <summary>
        /// the thread the debug window runs in
        /// </summary>
        private Thread debugWindowThread;

        /// <summary>
        /// the glasses window
        /// </summary>
        private GlassesWindow glassesWindow;

        /// <summary>
        /// true iff the glasses window is enabled
        /// </summary>
        private bool glassesWindowEnabled;

        /// <summary>
        /// the thread the glasses window runs in
        /// </summary>
        private Thread glassesWindowThread;

        /// <summary>
        /// the input handler
        /// </summary>
        private InputHandler inputHandler;

        /// <summary>
        /// the input provider
        /// </summary>
        private IInputProvider inputProvider;

        /// <summary>
        /// the tuio communicator
        /// </summary>
        private TuioCommunicatorHack tuioCommunicatorHack;
        private TuioCommunicator tuioCommunicator;

        /// <summary>
        /// true iff the tuio server is enabled
        /// </summary>
        private bool tuioEnabled;

        /// <summary>
        /// the ip address for the tuio server
        /// </summary>
        private String tuioIP;

        /// <summary>
        /// the port for the tuio server
        /// </summary>
        private Int16 tuioPort;

        /// <summary>
        /// Parses the command line arguments and depending on them creates the input, tuio and window objects.
        /// </summary>
        /// <param name="args">command line arguments</param>
        public BBIWARG(string[] args)
        {
            handleArgs(args);
            

            debugWindowEnabled = Parameters.DebugWindowEnabled;
            glassesWindowEnabled = Parameters.GlassesWindowEnabled && Screen.AllScreens.Count() >= 2;

            // inputProvider
            createInputProvider();
            inputProvider.initialize();
            inputProvider.DeviceStartedEvent += handleDeviceStartedEvent;

            // inputHandler
            inputHandler = new InputHandler(inputProvider);

            // tuioCommunicator
            if (tuioEnabled)
            {
                tuioCommunicatorHack = new TuioCommunicatorHack(tuioIP, tuioPort);
                inputHandler.NewProcessedFrameEvent += tuioCommunicatorHack.handleNewFrameData;

                tuioCommunicator = new TuioCommunicator("127.0.0.1", 3333);
                inputHandler.NewProcessedFrameEvent += tuioCommunicator.handleNewFrameData;
            }

            // debug output
            if (debugWindowEnabled)
                debugWindowThread = new Thread(new ThreadStart(debugWindowThreadStart));

            // glasses output
            if (glassesWindowEnabled)
                glassesWindowThread = new Thread(new ThreadStart(glassesWindowThreadStart));
        }

        /// <summary>
        /// Runs the main program.
        /// </summary>
        public void run()
        {
            inputProvider.start();
        }

        /// <summary>
        /// Starts the program.
        /// </summary>
        /// <param name="args">command line arguments</param>
        private static void Main(string[] args)
        {
            System.ServiceModel.ServiceHost host = new System.ServiceModel.ServiceHost(typeof(API.Server));
            host.Open();

            BBIWARG program = new BBIWARG(args);
            program.run();
        }

        /// <summary>
        /// Creates the input provider.
        /// </summary>
        private void createInputProvider()
        {
            if (Parameters.InputSource == InputType.Movie)
                inputProvider = new VideoInputProvider(Parameters.InputMoviePath);
            else if (Parameters.InputSource == InputType.DS325)
                inputProvider = new InputProviderIisu();
            else
                inputProvider = new InputProviderIntel();
        }

        /// <summary>
        /// Runs the debug window in its own thread.
        /// </summary>
        private void debugWindowThreadStart()
        {
            debugWindow = new DebugWindow(inputProvider, inputHandler, Parameters.DebugWindowTitle, Parameters.DebugWindowUpdateIntervall);
            Application.Run(debugWindow);
        }

        /// <summary>
        /// Runs the glasses window in its own thread.
        /// </summary>
        private void glassesWindowThreadStart()
        {
            glassesWindow = new GlassesWindow(inputProvider, inputHandler, Parameters.GlassesWindowTitle, Screen.AllScreens[1], Parameters.GlassesWindowUpdateInterval);
            Application.Run(glassesWindow);
        }

        /// <summary>
        /// Parses the command line arguments and sets parameters depending on them.
        /// </summary>
        /// <param name="args">command line arguments</param>
        private void handleArgs(String[] args)
        {
            if (args.Length > 0)
            {
                tuioEnabled = true;
                String ipString = null;
                String portString = null;
                if (args.Length == 2)
                {
                    ipString = args[0];
                    portString = args[1];
                }
                else if (args.Length == 1)
                {
                    String[] parts = args[0].Split(':');
                    ipString = parts[0];
                    if (parts.Length == 2)
                        portString = parts[1];
                }

                while (!TuioCommunicatorHack.tryParseIPAddress(ipString, out tuioIP))
                {
                    Console.WriteLine("Please insert the TUIO-Client's IP (Default is " + Parameters.TuioDefaultIP + "):");
                    ipString = Console.ReadLine();
                }
                Console.WriteLine("TUIO-IP is set to:" + tuioIP);

                while (!TuioCommunicatorHack.tryParsePort(portString, out tuioPort))
                {
                    Console.WriteLine("Please insert the TUIO-Client's Port (Default is " + Parameters.TuioDefaultPort + "):");
                    portString = Console.ReadLine();
                }
                Console.WriteLine("TUIO-PORT is set to:" + tuioPort);
            }
            else
            {
                tuioEnabled = Parameters.TuioEnabledByDefault;
                tuioIP = Parameters.TuioDefaultIP;
                tuioPort = Parameters.TuioDefaultPort;
            }
        }

        /// <summary>
        /// Handles the event that the device has started by starting the enabled windows.
        /// </summary>
        /// <param name="sender">event sender</param>
        /// <param name="e">event arguments</param>
        private void handleDeviceStartedEvent(object sender, EventArgs e)
        {
            if (debugWindowEnabled)
                debugWindowThread.Start();

            if (glassesWindowEnabled)
                glassesWindowThread.Start();
        }
    }
}