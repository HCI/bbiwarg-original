﻿using OSC.NET;
using System;
using System.Collections.Generic;

// using System.Runtime.InteropServices;

namespace TUIO
{
    /// <summary>
    /// Tuio server class to send tuio cursors and objects to tuio clients.
    /// </summary>
    internal class TuioServer
    {
        /// <summary>
        /// the maximum packet size
        /// </summary>
        private const int MAX_PACKET_SIZE = 65535 - 8;

        /// <summary>
        /// current tuio time
        /// </summary>
        private TuioTime currentFrameTime;

        /// <summary>
        /// list of generated cursors
        /// </summary>
        private List<TuioCursor> cursorList;

        /// <summary>
        /// server ip address
        /// </summary>
        private String host = "127.0.0.1";

        /// <summary>
        /// list of all generated objects
        /// </summary>
        private List<TuioObject> objectList;

        /// <summary>
        /// server port
        /// </summary>
        private int port = 3333;

        /// <summary>
        /// tuio session id
        /// </summary>
        private long sessionID = 0;

        /// <summary>
        /// transmitter to send packages over udp
        /// </summary>
        private OSCTransmitter transmitter = null;

        /// <summary>
        /// list of cursors updated in the current frame
        /// </summary>
        private List<TuioCursor> updatedCursorList;

        /// <summary>
        /// list of objects updated in the current frame
        /// </summary>
        private List<TuioObject> updatedObjectList;

        /// <summary>
        /// Creates a tuio server with default ip and port.
        /// </summary>
        public TuioServer()
        {
            init();
        }

        /// <summary>
        /// Creates a tuio server with default ip.
        /// </summary>
        /// <param name="port">server port</param>
        public TuioServer(int port)
        {
            this.port = port;
            init();
        }

        /// <summary>
        /// Creates a tuio server.
        /// </summary>
        /// <param name="host">server ip</param>
        /// <param name="port">server port</param>
        public TuioServer(String host, int port)
        {
            this.host = host;
            this.port = port;
            init();
        }

        /// <summary>
        /// Adds and returns a tuio cursor.
        /// </summary>
        /// <param name="xp">x position of the cursor</param>
        /// <param name="yp">y position of the cursor</param>
        /// <returns>the added cursor</returns>
        public TuioCursor addTuioCursor(float xp, float yp)
        {
            TuioCursor tcur = new TuioCursor(sessionID, cursorList.Count, xp, yp);
            cursorList.Add(tcur);
            updatedCursorList.Add(tcur);

            sessionID++;
            return tcur;
        }

        /// <summary>
        /// Adds and returns tuio object.
        /// </summary>
        /// <param name="xp">x position of the object</param>
        /// <param name="yp">y position of the object</param>
        /// <param name="angle">angle of the object</param>
        /// <returns>the added object</returns>
        public TuioObject addTuioObject(float xp, float yp, float angle)
        {
            TuioObject tobj = new TuioObject(sessionID, objectList.Count, xp, yp, angle);
            objectList.Add(tobj);
            updatedObjectList.Add(tobj);

            sessionID++;
            return tobj;
        }

        /// <summary>
        /// Closes the transmission.
        /// </summary>
        public void close()
        {
            transmitter.Close();
        }

        /// <summary>
        /// Sends cursors and objects which were updated in the current frame.
        /// </summary>
        public void commitFrame()
        {
            sendMessage(updatedCursorList, updatedObjectList);
        }

        /// <summary>
        /// Initializes cursor and object lists for sending information for a new frame.
        /// </summary>
        public void initFrame()
        {
            currentFrameTime = TuioTime.getSessionTime();
            updatedCursorList = new List<TuioCursor>();
            updatedObjectList = new List<TuioObject>();
        }

        /// <summary>
        /// Removes a tuio cursor.
        /// </summary>
        /// <param name="tcur">the cursor to remove</param>
        public void removeTuioCursor(TuioCursor tcur)
        {
            cursorList.Remove(tcur);
        }

        /// <summary>
        /// Removes a tuio object.
        /// </summary>
        /// <param name="tobj">the object to remove</param>
        public void removeTuioObject(TuioObject tobj)
        {
            objectList.Remove(tobj);
        }

        /// <summary>
        /// Sends all cursors and objects.
        /// </summary>
        public void sendFullMessages()
        {
            sendMessage(cursorList, objectList);
        }

        /// <summary>
        /// Updates a tui cursor.
        /// </summary>
        /// <param name="tcur">the cursor to update</param>
        /// <param name="xp">new x position</param>
        /// <param name="yp">new y position</param>
        public void updateTuioCursor(TuioCursor tcur, float xp, float yp)
        {
            tcur.update(currentFrameTime, xp, yp);
            if (!updatedCursorList.Contains(tcur))
                updatedCursorList.Add(tcur);
        }

        /// <summary>
        /// Updates a tuio object.
        /// </summary>
        /// <param name="tobj">the object to update</param>
        /// <param name="xp">new x position</param>
        /// <param name="yp">new y position</param>
        public void updateTuioObject(TuioObject tobj, float xp, float yp)
        {
            tobj.update(currentFrameTime, xp, yp);
            if (!updatedObjectList.Contains(tobj))
                updatedObjectList.Add(tobj);
        }

        /// <summary>
        /// Updates a tuio object.
        /// </summary>
        /// <param name="tobj">the object to update</param>
        /// <param name="xp">new x position</param>
        /// <param name="yp">new y position</param>
        public void updateTuioObject(TuioObject tobj, float xp, float yp, float angle)
        {
            tobj.update(currentFrameTime, xp, yp, angle);
            if (!updatedObjectList.Contains(tobj))
                updatedObjectList.Add(tobj);
        }

        /// <summary>
        /// Adds a tuio cursor alive message to a packet.
        /// </summary>
        /// <param name="packet">packet to add the message to</param>
        private void addAliveCursorMessagesToBundle(OSCBundle packet)
        {
            OSCMessage mssg = new OSCMessage("/tuio/2Dcur");
            mssg.Append("alive");
            for (int i = 0; i < cursorList.Count; i++)
            {
                mssg.Append((Int32)cursorList[i].getSessionID());
            }
            packet.Append(mssg);
        }

        /// <summary>
        /// Adds a tuio object alive message to a packet.
        /// </summary>
        /// <param name="packet">packet to add the message to</param>
        private void addAliveObjectMessagesToBundle(OSCBundle packet)
        {
            OSCMessage mssg = new OSCMessage("/tuio/2Dobj");
            mssg.Append("alive");
            for (int i = 0; i < objectList.Count; i++)
            {
                mssg.Append((Int32)objectList[i].getSessionID());
            }
            packet.Append(mssg);
        }

        /// <summary>
        /// Initializes the server, cursors and objects.
        /// </summary>
        private void init()
        {
            TuioTime.initSession();
            cursorList = new List<TuioCursor>();
            objectList = new List<TuioObject>();
            transmitter = new OSCTransmitter(host, port);
        }

        /// <summary>
        /// Sends cursors and objects.
        /// </summary>
        /// <param name="cursorList">list of cursors to send</param>
        /// <param name="objectList">list of objects to send</param>
        private void sendMessage(List<TuioCursor> cursorList, List<TuioObject> objectList)
        {
            OSCBundle packet = new OSCBundle();
            OSCMessage currentMessage;

            // cursors
            addAliveCursorMessagesToBundle(packet);
            TuioCursor tcur;
            for (int i = 0; i < cursorList.Count; i++)
            {
                tcur = cursorList[i];
                currentMessage = new OSCMessage("/tuio/2Dcur");
                currentMessage.Append("set");
                currentMessage.Append((Int32)tcur.getSessionID());
                currentMessage.Append(tcur.getX());
                currentMessage.Append(tcur.getY());
                currentMessage.Append(tcur.getXSpeed());
                currentMessage.Append(tcur.getYSpeed());
                currentMessage.Append(tcur.getMotionAccel());
                packet.Append(currentMessage);
            }
            currentMessage = new OSCMessage("/tuio/2Dcur");
            currentMessage.Append("fseq");
            currentMessage.Append(-1); //sequence_id; actually -1 stands for redundant bundle
            packet.Append(currentMessage);
            transmitter.Send(packet);

            // objects
            packet = new OSCBundle();

            addAliveObjectMessagesToBundle(packet);
            TuioObject tobj;
            for (int i = 0; i < objectList.Count; i++)
            {
                tobj = objectList[i];
                currentMessage = new OSCMessage("/tuio/2Dobj");
                currentMessage.Append("set");
                currentMessage.Append((Int32)tobj.getSessionID());
                currentMessage.Append(tobj.getSymbolID());
                currentMessage.Append(tobj.getX());
                currentMessage.Append(tobj.getY());
                currentMessage.Append(tobj.getAngle());
                currentMessage.Append(tobj.getXSpeed());
                currentMessage.Append(tobj.getYSpeed());
                currentMessage.Append(tobj.getRotationSpeed());
                currentMessage.Append(tobj.getMotionAccel());
                currentMessage.Append(tobj.getRotationAccel());

                packet.Append(currentMessage);
            }
            currentMessage = new OSCMessage("/tuio/2Dobj");
            currentMessage.Append("fseq");
            currentMessage.Append(-1); //sequence_id; actually -1 stands for redundant bundle
            packet.Append(currentMessage);

            transmitter.Send(packet);
        }
    }
}